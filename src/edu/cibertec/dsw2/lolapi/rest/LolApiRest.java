package edu.cibertec.dsw2.lolapi.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.cibertec.dsw2.lolapi.bean.RespuestaErrorBean;
import edu.cibertec.dsw2.lolapi.bean.RespuestaOkBean;
import edu.cibertec.dsw2.lolapi.dao.LolDAO;
import edu.cibertec.dsw2.lolapi.dao.impl.LolDAOImpl;
import edu.cibertec.dsw2.lolapi.dto.PersonajeCountersDTO;
import edu.cibertec.dsw2.lolapi.dto.PersonajeDTO;
import edu.cibertec.dsw2.lolapi.dto.ProfesionalCarrilDTO;
import edu.cibertec.dsw2.lolapi.dto.ProfesionalDTO;
import edu.cibertec.dsw2.lolapi.dto.RecomendacionesRunasPerProDTO;
import edu.cibertec.dsw2.lolapi.dto.RegionesDTO;
import edu.cibertec.dsw2.lolapi.dto.ResultadoDTO;
import edu.cibertec.dsw2.lolapi.dto.ResultadoItemsDTO;
import edu.cibertec.dsw2.lolapi.dto.ResultadoPersonajesDTO;
import edu.cibertec.dsw2.lolapi.dto.RunasRecomendadasDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;


@Api(tags= {"cibertec-proyecto-dsw2-lolapi"})
@Path("lolapirest")
@Produces(MediaType.APPLICATION_JSON)
@ApiResponses(value = {
		@ApiResponse(code=100, message="Continue"),
		@ApiResponse(code=101, message="Switching Protocols"),
		@ApiResponse(code=102, message="Processing"),
		@ApiResponse(code=200, message="OK"),
		@ApiResponse(code=201, message="Created"),
		@ApiResponse(code=202, message="Accepted"),
		@ApiResponse(code=203, message="Non-authoritative Information"),
		@ApiResponse(code=204, message="No Content"),
		@ApiResponse(code=205, message="Reset Content"),
		@ApiResponse(code=206, message="Partial Content"),
		@ApiResponse(code=207, message="Multi-Status"),
		@ApiResponse(code=208, message="Already Reported"),
		@ApiResponse(code=226, message="IM Used"),
		@ApiResponse(code=300, message="Multiple Choices"),
		@ApiResponse(code=301, message="Moved Permanently"),
		@ApiResponse(code=302, message="Found"),
		@ApiResponse(code=303, message="See Other"),
		@ApiResponse(code=304, message="Not Modified"),
		@ApiResponse(code=305, message="Use Proxy"),
		@ApiResponse(code=307, message="Temporary Redirect"),
		@ApiResponse(code=308, message="Permanent Redirect"),
		@ApiResponse(code=400, message="Bad Request"),
		@ApiResponse(code=401, message="Unauthorized"),
		@ApiResponse(code=402, message="Payment Required"),
		@ApiResponse(code=403, message="Forbidden"),
		@ApiResponse(code=404, message="Not Found"),
		@ApiResponse(code=405, message="Method Not Allowed"),
		@ApiResponse(code=406, message="Not Acceptable"),
		@ApiResponse(code=407, message="Proxy Authentication Required"),
		@ApiResponse(code=408, message="Request Timeout"),
		@ApiResponse(code=409, message="Conflict"),
		@ApiResponse(code=410, message="Gone"),
		@ApiResponse(code=411, message="Length Required"),
		@ApiResponse(code=412, message="Precondition Failed"),
		@ApiResponse(code=413, message="Payload Too Large"),
		@ApiResponse(code=414, message="Request-URI Too Long"),
		@ApiResponse(code=415, message="Unsupported Media Type"),
		@ApiResponse(code=416, message="Requested Range Not Satisfiable"),
		@ApiResponse(code=417, message="Expectation Failed"),
		@ApiResponse(code=418, message="I'm a teapot"),
		@ApiResponse(code=421, message="Misdirected Request"),
		@ApiResponse(code=422, message="Unprocessable Entity"),
		@ApiResponse(code=423, message="Locked"),
		@ApiResponse(code=424, message="Failed Dependency"),
		@ApiResponse(code=426, message="Upgrade Required"),
		@ApiResponse(code=428, message="Precondition Required"),
		@ApiResponse(code=429, message="Too Many Requests"),
		@ApiResponse(code=431, message="Request Header Fields Too Large"),
		@ApiResponse(code=444, message="Connection Closed Without Response"),
		@ApiResponse(code=451, message="Unavailable For Legal Reasons"),
		@ApiResponse(code=499, message="Client Closed Request"),
		@ApiResponse(code=500, message="Internal Server Error"),
		@ApiResponse(code=501, message="Not Implemented"),
		@ApiResponse(code=502, message="Bad Gateway"),
		@ApiResponse(code=503, message="Service Unavailable"),
		@ApiResponse(code=504, message="Gateway Timeout"),
		@ApiResponse(code=505, message="HTTP Version Not Supported"),
		@ApiResponse(code=506, message="Variant Also Negotiates"),
		@ApiResponse(code=507, message="Insufficient Storage"),
		@ApiResponse(code=508, message="Loop Detected"),
		@ApiResponse(code=510, message="Not Extended"),
		@ApiResponse(code=511, message="Network Authentication Required"),
		@ApiResponse(code=599, message="Network Connect Timeout Error")
	})
public class LolApiRest {	
	
	@ApiOperation(value="Obtiene una lista de Regiones", consumes=MediaType.APPLICATION_JSON,produces=MediaType.APPLICATION_JSON)
	@GET
	@Path("/listadoRegiones")
	@Produces(MediaType.APPLICATION_JSON)
	public ResultadoDTO regionesActuales() {
		System.out.println("Inicio - regionesActuales");
		
		ResultadoDTO resultado = new ResultadoDTO();
		LolDAO dao = new LolDAOImpl();
		
		List<RegionesDTO> lst = dao.obtenerRegiones();
		resultado.setResultadoRegiones(lst);
		return resultado;
	}
	
	@ApiOperation(value="Obtiene lista de personajes disponibles en el juego", consumes=MediaType.APPLICATION_JSON,produces=MediaType.APPLICATION_JSON)
	@GET
	@Path("/listadoPersonajes")
	@Produces(MediaType.APPLICATION_JSON)
	public ResultadoPersonajesDTO listaPersonajesLeagueOfLegends() {
		System.out.println("Inicio - listaPersonajesLeagueOfLegends");
		
		ResultadoPersonajesDTO resultado = new ResultadoPersonajesDTO();
		LolDAO dao = new LolDAOImpl();
		
		List<PersonajeDTO> listadoPersonajeDTO = dao.obtenerPersonajes();
		resultado.setResultadoPersonajes(listadoPersonajeDTO);
		return resultado;
	}
	
	@ApiOperation(value="Obtiene Profesionales por Carril Preferido", consumes=MediaType.APPLICATION_JSON,produces=MediaType.APPLICATION_JSON)
	@GET
	@Path("/profesionalesPorCarril/{carrilSeleccionado}")
	@Produces(MediaType.APPLICATION_JSON)
	public ProfesionalCarrilDTO listaProfesionalesPorCarril(@PathParam("carrilSeleccionado") String carrilSel) {
		System.out.println("Inicio - listaProfesionalesPorCarril");
		System.out.println("Carril Seleccionado: " + carrilSel);

		ProfesionalCarrilDTO profesionalCarrilDTO = new ProfesionalCarrilDTO();
		LolDAO dao = new LolDAOImpl();
		profesionalCarrilDTO = dao.obtenerProfesionalesPorCarril(carrilSel);
		return profesionalCarrilDTO;
	}
	
	@ApiOperation(value="Lista de objetos disponibles en la temporada actual", consumes=MediaType.APPLICATION_JSON,produces=MediaType.APPLICATION_JSON)
	@GET
	@Path("/listadoItems")
	@Produces(MediaType.APPLICATION_JSON)
	public ResultadoItemsDTO listadoItemsLeagueOfLegends() {
		System.out.println("Inicio - listadoItemsLeagueOfLegends");
		
		ResultadoItemsDTO resultadoItemsDTO = new ResultadoItemsDTO();
		LolDAO dao = new LolDAOImpl();
		resultadoItemsDTO = dao.obtenerItems();
		System.out.println("Fin - listadoItemsLeagueOfLegends");
		return resultadoItemsDTO;
	}
	
	@ApiOperation(value="Runas recomendadas del Personaje Favorito del Profesional", consumes=MediaType.APPLICATION_JSON,produces=MediaType.APPLICATION_JSON)
	@GET
	@Path("/recomendacionRunasPersonajeProfesional/{personaje}/{profesional}")
	@Produces(MediaType.APPLICATION_JSON)
	public RecomendacionesRunasPerProDTO listaRecomendacionesRunasPerPro(@PathParam("personaje") String personaje, @PathParam("profesional") String profesional) {
		System.out.println("Inicio - listaRecomendacionesRunasPerPro");
		System.out.println("Personaje: " + personaje);
		System.out.println("Profesional: " + profesional);

		RecomendacionesRunasPerProDTO recomendaciones = new RecomendacionesRunasPerProDTO();
		LolDAO dao = new LolDAOImpl();
		recomendaciones = dao.obtenerRecomendacionesRunasPerPro(personaje, profesional);
		return recomendaciones;
	}
	
	@ApiOperation(value="Obtiene listado de items por Letra seleccionada", consumes=MediaType.APPLICATION_JSON,produces=MediaType.APPLICATION_JSON)
	@GET
	@Path("/listadoItemsPorLetra/{letra}")
	@Produces(MediaType.APPLICATION_JSON)
	public ResultadoItemsDTO listadoItemsLeagueOfLegendsPorLetra(@PathParam("letra") String letra) {
		System.out.println("Inicio - listadoItemsLeagueOfLegendsPorLetra");
		
		ResultadoItemsDTO resultadoItemsDTO = new ResultadoItemsDTO();
		LolDAO dao = new LolDAOImpl();
		resultadoItemsDTO = dao.obtenerItemsPorLetra(letra);
		System.out.println("Fin - listadoItemsLeagueOfLegendsPorLetra");
		return resultadoItemsDTO;
	}
	
	@ApiOperation(value="Registra regi�n que los desarrolladores habilitaron recientemente", consumes=MediaType.APPLICATION_JSON,produces=MediaType.APPLICATION_JSON)
	@POST
	@Path("/registrarRegion")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response registrarRegion(RegionesDTO region) {
		System.out.println("Inicio - registrarRegion");

		try {
			LolDAO dao = new LolDAOImpl();

			if (region == null) {
				System.out.println("Se esta tratando de insertar vacios o nulos");
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(
						new RespuestaErrorBean<RegionesDTO>("0", "Existen nulos y/o vacios al realizar el registro"))
						.build();
			} else {
				RegionesDTO reg = dao.insertarRegion(region);
				if (LolDAOImpl.indDuplicado == 1) {
					return Response.status(Response.Status.CONFLICT).entity(new RespuestaOkBean<RegionesDTO>("-1",
							"Ya se ha registrado!!!, Error de llave duplicada", region)).build();
				} else {
					return Response.status(Response.Status.OK).entity(
							new RespuestaOkBean<RegionesDTO>("1", "Se realizo el registro correctamente", region))
							.build();
				}
			}
		} catch (Exception e) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity(new RespuestaErrorBean<RegionesDTO>("0", "Error")).build();
		}
	}
	
	@ApiOperation(value="Registra a Profesionales que ofreceran servicios de Coaching", consumes=MediaType.APPLICATION_JSON,produces=MediaType.APPLICATION_JSON)
	@POST
	@Path("/registrarProfesional")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response registrarProfesional(ProfesionalDTO profesional) {
		System.out.println("Inicio - registrarProfesional");

		try {
			LolDAO dao = new LolDAOImpl();

			if (profesional == null) {
				System.out.println("Se esta tratando de insertar vacios o nulos");
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(
						new RespuestaErrorBean<RegionesDTO>("0", "Existen nulos y/o vacios al realizar el registro"))
						.build();
			} else {
				ProfesionalDTO pro = dao.insertarProfesional(profesional);
				if (LolDAOImpl.indDuplicado == 1) {
					return Response.status(Response.Status.CONFLICT).entity(
							new RespuestaOkBean<>("-1", "Ya se ha registrado!!!, Error de llave duplicada", null))
							.build();
				} else {
					return Response.status(Response.Status.OK).entity(new RespuestaOkBean<ProfesionalDTO>("1",
							"Se realizo el registro correctamente", profesional)).build();
				}
			}
		} catch (Exception e) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity(new RespuestaErrorBean<ProfesionalDTO>("0", "Error")).build();
		}
	}

	@ApiOperation(value="Registra runas que los Profesionales recomiendan para personajes seleccionados", consumes=MediaType.APPLICATION_JSON,produces=MediaType.APPLICATION_JSON)
	@POST
	@Path("/registrarRunasRecomendadasProfesionalPersonaje")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response registrarRunasRecomendadasProfesionalPersonaje(RunasRecomendadasDTO runasRec) {
		System.out.println("Inicio - registrarRunasRecomendadasProfesionalPersonaje");

		try {
			LolDAO dao = new LolDAOImpl();

			if (runasRec == null) {
				System.out.println("Se esta tratando de insertar vacios o nulos");
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
						.entity(new RespuestaErrorBean<RegionesDTO>("0",
								"Existen nulos y/o vacios al realizar el registro de Runas Recomendadas de Personaje Por un Profesional"))
						.build();
			} else {
				RunasRecomendadasDTO runasRecomendadasDTO = dao.insertarRunasRecomendadas(runasRec);
				if (LolDAOImpl.indDuplicado == 1) {
					return Response.status(Response.Status.CONFLICT).entity(
							new RespuestaOkBean<>("-1", "Ya se ha registrado!!!, Error de llave duplicada", null))
							.build();
				} else {
					System.out.println("Fin - registrarRunasRecomendadasProfesionalPersonaje");
					return Response.status(Response.Status.OK).entity(new RespuestaOkBean<RunasRecomendadasDTO>("1",
							"Se realizo el registro correctamente de runas recomendadas", runasRec)).build();
				}
			}
		} catch (Exception e) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity(new RespuestaErrorBean<ProfesionalDTO>("0", "Error")).build();
		}
	}

	@ApiOperation(value="Muestra informaci�n de los personajes que le hacen Counter a un personaje seleccionado", consumes=MediaType.APPLICATION_JSON,produces=MediaType.APPLICATION_JSON)
	@POST
	@Path("/DetalleDatosCounter")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public Response listarDetalleCounterPersonaje(String nombrePersonaje) {
		System.out.println("Inicio - listarDetalleCounterPersonaje");
		System.out.println("nombrePersonaje:" + nombrePersonaje);

		try {

			if (nombrePersonaje == null || nombrePersonaje == "") {
				System.out.println("El nombre del personaje se encuentra vac�o o nulo");
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(
						new RespuestaErrorBean<PersonajeCountersDTO>("0", "El nombre del personaje se encuentra vac�o"))
						.build();
			} else {
				LolDAO dao = new LolDAOImpl();

				PersonajeCountersDTO personajeCountersDTO = dao.obtenerCounterPersonajes(nombrePersonaje);

				if (personajeCountersDTO == null) {
					return Response.status(Response.Status.OK)
							.entity(new RespuestaErrorBean<>("0", "No se encontraron resultados")).build();
				} else {
					System.out.println("Fin - listarDetalleCounterPersonaje");
					return Response.status(Response.Status.OK).entity(new RespuestaOkBean<PersonajeCountersDTO>("1",
							"Se realizo la busqueda correctamente de counters", personajeCountersDTO)).build();
				}

			}
		} catch (Exception e) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity(new RespuestaErrorBean<PersonajeCountersDTO>("0", "Error")).build();
		}
	}

	@ApiOperation(value="Actualiza informaci�n de personajes de acuerdo a los ultimos parches del juego", consumes=MediaType.APPLICATION_JSON,produces=MediaType.APPLICATION_JSON)
	@POST
	@Path("/actualizarPersonaje")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response actualizarPersonaje(PersonajeDTO personaje) {

		System.out.println("Inicio - actualizarPersonaje");

		try {
			LolDAO dao = new LolDAOImpl();

			dao.actualizarPersonaje(personaje);
			return Response.status(Response.Status.OK).entity(new RespuestaOkBean<PersonajeDTO>("1",
					"Se realizo la actualizacion de personaje correctamente", personaje)).build();

		} catch (Exception e) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity(new RespuestaErrorBean<ProfesionalDTO>("0", "Error")).build();
		}
	}

}
