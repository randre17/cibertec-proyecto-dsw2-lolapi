package edu.cibertec.dsw2.lolapi.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ItemsDTO implements Serializable {

	private long idItem;
	private String nomItem;
	private String desItem;
	private String statsItem;
	private String imgItem;

	public ItemsDTO(long idItem, String nomItem, String desItem, String statsItem, String imgItem) {
		super();
		this.idItem = idItem;
		this.nomItem = nomItem;
		this.desItem = desItem;
		this.statsItem = statsItem;
		this.imgItem = imgItem;
	}

	public ItemsDTO() {
	}

	public long getIdItem() {
		return idItem;
	}

	public void setIdItem(long idItem) {
		this.idItem = idItem;
	}

	public String getNomItem() {
		return nomItem;
	}

	public void setNomItem(String nomItem) {
		this.nomItem = nomItem;
	}

	public String getDesItem() {
		return desItem;
	}

	public void setDesItem(String desItem) {
		this.desItem = desItem;
	}

	public String getStatsItem() {
		return statsItem;
	}

	public void setStatsItem(String statsItem) {
		this.statsItem = statsItem;
	}

	public String getImgItem() {
		return imgItem;
	}

	public void setImgItem(String imgItem) {
		this.imgItem = imgItem;
	}

}
