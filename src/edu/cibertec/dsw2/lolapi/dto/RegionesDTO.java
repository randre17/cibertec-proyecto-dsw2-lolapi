package edu.cibertec.dsw2.lolapi.dto;

import java.io.Serializable;

public class RegionesDTO implements Serializable {

	private long idRegion;
	private String desRegion;
	private String abvRegion;

	public RegionesDTO() {
	}

	public RegionesDTO(long idRegion, String desRegion, String abvRegion) {
		super();
		this.idRegion = idRegion;
		this.desRegion = desRegion;
		this.abvRegion = abvRegion;
	}

	public long getIdRegion() {
		return idRegion;
	}

	public void setIdRegion(long idRegion) {
		this.idRegion = idRegion;
	}

	public String getDesRegion() {
		return desRegion;
	}

	public void setDesRegion(String desRegion) {
		this.desRegion = desRegion;
	}

	public String getAbvRegion() {
		return abvRegion;
	}

	public void setAbvRegion(String abvRegion) {
		this.abvRegion = abvRegion;
	}
	
	

}
