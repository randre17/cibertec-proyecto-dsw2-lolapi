package edu.cibertec.dsw2.lolapi.dto;

import java.util.List;

public class ProfesionalCarrilDTO {

	private int cantidadProfesionales;
	private String carrilIngresado;
	private String fechaBusqueda;
	private List<ProfesionalDTO> listaProfesionales;

	public ProfesionalCarrilDTO(int cantidadProfesionales, String carrilIngresado, String fechaBusqueda,
			List<ProfesionalDTO> listaProfesionales) {
		super();
		this.cantidadProfesionales = cantidadProfesionales;
		this.carrilIngresado = carrilIngresado;
		this.fechaBusqueda = fechaBusqueda;
		this.listaProfesionales = listaProfesionales;
	}

	public ProfesionalCarrilDTO() {
	}

	public int getCantidadProfesionales() {
		return cantidadProfesionales;
	}

	public void setCantidadProfesionales(int cantidadProfesionales) {
		this.cantidadProfesionales = cantidadProfesionales;
	}

	public String getCarrilIngresado() {
		return carrilIngresado;
	}

	public void setCarrilIngresado(String carrilIngresado) {
		this.carrilIngresado = carrilIngresado;
	}

	public String getFechaBusqueda() {
		return fechaBusqueda;
	}

	public void setFechaBusqueda(String fechaBusqueda) {
		this.fechaBusqueda = fechaBusqueda;
	}

	public List<ProfesionalDTO> getListaProfesionales() {
		return listaProfesionales;
	}

	public void setListaProfesionales(List<ProfesionalDTO> listaProfesionales) {
		this.listaProfesionales = listaProfesionales;
	}

}
