package edu.cibertec.dsw2.lolapi.dto;

import java.util.List;

public class ResultadoItemsDTO {

	private int cantidadItems;
	private String fechaBusqueda;
	private List<ItemsDTO> listaItems;

	public ResultadoItemsDTO(int cantidadItems, String fechaBusqueda, List<ItemsDTO> listaItems) {
		super();
		this.cantidadItems = cantidadItems;
		this.fechaBusqueda = fechaBusqueda;
		this.listaItems = listaItems;
	}

	public ResultadoItemsDTO() {
	}

	public int getCantidadItems() {
		return cantidadItems;
	}

	public void setCantidadItems(int cantidadItems) {
		this.cantidadItems = cantidadItems;
	}

	public String getFechaBusqueda() {
		return fechaBusqueda;
	}

	public void setFechaBusqueda(String fechaBusqueda) {
		this.fechaBusqueda = fechaBusqueda;
	}

	public List<ItemsDTO> getListaItems() {
		return listaItems;
	}

	public void setListaItems(List<ItemsDTO> listaItems) {
		this.listaItems = listaItems;
	}

}
