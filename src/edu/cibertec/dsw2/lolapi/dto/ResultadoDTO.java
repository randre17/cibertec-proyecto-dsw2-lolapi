package edu.cibertec.dsw2.lolapi.dto;

import java.io.Serializable;
import java.util.List;

public class ResultadoDTO implements Serializable {

	private List<RegionesDTO> resultadoRegiones;

	public List<RegionesDTO> getResultadoRegiones() {
		return resultadoRegiones;
	}

	public void setResultadoRegiones(List<RegionesDTO> resultadoRegiones) {
		this.resultadoRegiones = resultadoRegiones;
	}

}
