package edu.cibertec.dsw2.lolapi.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class PersonajeCountersDTO implements Serializable {

	private String personajeIngresado;
	private String personajeGanaLineaContra;
	private PersonajeDTO detallePersonajeGanaLineaContra;
	private String personajePierdeLineaContra;
	private PersonajeDTO detallePersonajePierdeLineaContra;
	private String personajeMasFuerteContra;
	private PersonajeDTO detallePersonajeMasFuerteContra;
	private String personajeGanaMasContra;
	private PersonajeDTO detallePersonajeGanaMasContra;
	private String personajePierdeMasContra;
	private PersonajeDTO detallePersonajePierdeMasContra;

	public PersonajeCountersDTO(String personajeIngresado, String personajeGanaLineaContra,
			PersonajeDTO detallePersonajeGanaLineaContra, String personajePierdeLineaContra,
			PersonajeDTO detallePersonajePierdeLineaContra, String personajeMasFuerteContra,
			PersonajeDTO detallePersonajeMasFuerteContra, String personajeGanaMasContra,
			PersonajeDTO detallePersonajeGanaMasContra, String personajePierdeMasContra,
			PersonajeDTO detallePersonajePierdeMasContra) {
		super();
		this.personajeIngresado = personajeIngresado;
		this.personajeGanaLineaContra = personajeGanaLineaContra;
		this.detallePersonajeGanaLineaContra = detallePersonajeGanaLineaContra;
		this.personajePierdeLineaContra = personajePierdeLineaContra;
		this.detallePersonajePierdeLineaContra = detallePersonajePierdeLineaContra;
		this.personajeMasFuerteContra = personajeMasFuerteContra;
		this.detallePersonajeMasFuerteContra = detallePersonajeMasFuerteContra;
		this.personajeGanaMasContra = personajeGanaMasContra;
		this.detallePersonajeGanaMasContra = detallePersonajeGanaMasContra;
		this.personajePierdeMasContra = personajePierdeMasContra;
		this.detallePersonajePierdeMasContra = detallePersonajePierdeMasContra;
	}

	public PersonajeCountersDTO() {
	}

	public String getPersonajeIngresado() {
		return personajeIngresado;
	}

	public void setPersonajeIngresado(String personajeIngresado) {
		this.personajeIngresado = personajeIngresado;
	}

	public String getPersonajeGanaLineaContra() {
		return personajeGanaLineaContra;
	}

	public void setPersonajeGanaLineaContra(String personajeGanaLineaContra) {
		this.personajeGanaLineaContra = personajeGanaLineaContra;
	}

	public PersonajeDTO getDetallePersonajeGanaLineaContra() {
		return detallePersonajeGanaLineaContra;
	}

	public void setDetallePersonajeGanaLineaContra(PersonajeDTO detallePersonajeGanaLineaContra) {
		this.detallePersonajeGanaLineaContra = detallePersonajeGanaLineaContra;
	}

	public String getPersonajePierdeLineaContra() {
		return personajePierdeLineaContra;
	}

	public void setPersonajePierdeLineaContra(String personajePierdeLineaContra) {
		this.personajePierdeLineaContra = personajePierdeLineaContra;
	}

	public PersonajeDTO getDetallePersonajePierdeLineaContra() {
		return detallePersonajePierdeLineaContra;
	}

	public void setDetallePersonajePierdeLineaContra(PersonajeDTO detallePersonajePierdeLineaContra) {
		this.detallePersonajePierdeLineaContra = detallePersonajePierdeLineaContra;
	}

	public String getPersonajeMasFuerteContra() {
		return personajeMasFuerteContra;
	}

	public void setPersonajeMasFuerteContra(String personajeMasFuerteContra) {
		this.personajeMasFuerteContra = personajeMasFuerteContra;
	}

	public PersonajeDTO getDetallePersonajeMasFuerteContra() {
		return detallePersonajeMasFuerteContra;
	}

	public void setDetallePersonajeMasFuerteContra(PersonajeDTO detallePersonajeMasFuerteContra) {
		this.detallePersonajeMasFuerteContra = detallePersonajeMasFuerteContra;
	}

	public String getPersonajeGanaMasContra() {
		return personajeGanaMasContra;
	}

	public void setPersonajeGanaMasContra(String personajeGanaMasContra) {
		this.personajeGanaMasContra = personajeGanaMasContra;
	}

	public PersonajeDTO getDetallePersonajeGanaMasContra() {
		return detallePersonajeGanaMasContra;
	}

	public void setDetallePersonajeGanaMasContra(PersonajeDTO detallePersonajeGanaMasContra) {
		this.detallePersonajeGanaMasContra = detallePersonajeGanaMasContra;
	}

	public String getPersonajePierdeMasContra() {
		return personajePierdeMasContra;
	}

	public void setPersonajePierdeMasContra(String personajePierdeMasContra) {
		this.personajePierdeMasContra = personajePierdeMasContra;
	}

	public PersonajeDTO getDetallePersonajePierdeMasContra() {
		return detallePersonajePierdeMasContra;
	}

	public void setDetallePersonajePierdeMasContra(PersonajeDTO detallePersonajePierdeMasContra) {
		this.detallePersonajePierdeMasContra = detallePersonajePierdeMasContra;
	}

	
}
