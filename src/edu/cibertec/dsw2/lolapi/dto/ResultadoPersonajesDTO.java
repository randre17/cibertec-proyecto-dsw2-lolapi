package edu.cibertec.dsw2.lolapi.dto;

import java.io.Serializable;
import java.util.List;

public class ResultadoPersonajesDTO implements Serializable {

	private List<PersonajeDTO> resultadoPersonajes;

	public List<PersonajeDTO> getResultadoPersonajes() {
		return resultadoPersonajes;
	}

	public void setResultadoPersonajes(List<PersonajeDTO> resultadoPersonajes) {
		this.resultadoPersonajes = resultadoPersonajes;
	}

}
