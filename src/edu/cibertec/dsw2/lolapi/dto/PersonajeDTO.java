package edu.cibertec.dsw2.lolapi.dto;

import java.io.Serializable;

public class PersonajeDTO implements Serializable {

	private long idPer;
	private String nomPer;
	private long idRolPer;
	private String linPri;
	private String linSec;
	private String vidaBase;
	private String regVidaBase;
	private String manaBase;
	private String regManaBase;
	private String armBase;
	private String resMagBase;
	private String danoFisBase;
	private String movPer;
	private String rangoPer;
	private String velAtaqueBasico;
	private String velAtaqueAdicional;
	private String enlaceOficial;

	public PersonajeDTO(long idPer, String nomPer, long idRolPer, String linPri, String linSec, String vidaBase,
			String regVidaBase, String manaBase, String regManaBase, String armBase, String resMagBase,
			String danoFisBase, String movPer, String rangoPer, String velAtaqueBasico, String velAtaqueAdicional,
			String enlaceOficial) {
		super();
		this.idPer = idPer;
		this.nomPer = nomPer;
		this.idRolPer = idRolPer;
		this.linPri = linPri;
		this.linSec = linSec;
		this.vidaBase = vidaBase;
		this.regVidaBase = regVidaBase;
		this.manaBase = manaBase;
		this.regManaBase = regManaBase;
		this.armBase = armBase;
		this.resMagBase = resMagBase;
		this.danoFisBase = danoFisBase;
		this.movPer = movPer;
		this.rangoPer = rangoPer;
		this.velAtaqueBasico = velAtaqueBasico;
		this.velAtaqueAdicional = velAtaqueAdicional;
		this.enlaceOficial = enlaceOficial;
	}

	public PersonajeDTO() {
	}

	public long getIdPer() {
		return idPer;
	}

	public void setIdPer(long idPer) {
		this.idPer = idPer;
	}

	public String getNomPer() {
		return nomPer;
	}

	public void setNomPer(String nomPer) {
		this.nomPer = nomPer;
	}

	public long getIdRolPer() {
		return idRolPer;
	}

	public void setIdRolPer(long idRolPer) {
		this.idRolPer = idRolPer;
	}

	public String getLinPri() {
		return linPri;
	}

	public void setLinPri(String linPri) {
		this.linPri = linPri;
	}

	public String getLinSec() {
		return linSec;
	}

	public void setLinSec(String linSec) {
		this.linSec = linSec;
	}

	public String getVidaBase() {
		return vidaBase;
	}

	public void setVidaBase(String vidaBase) {
		this.vidaBase = vidaBase;
	}

	public String getRegVidaBase() {
		return regVidaBase;
	}

	public void setRegVidaBase(String regVidaBase) {
		this.regVidaBase = regVidaBase;
	}

	public String getManaBase() {
		return manaBase;
	}

	public void setManaBase(String manaBase) {
		this.manaBase = manaBase;
	}

	public String getRegManaBase() {
		return regManaBase;
	}

	public void setRegManaBase(String regManaBase) {
		this.regManaBase = regManaBase;
	}

	public String getArmBase() {
		return armBase;
	}

	public void setArmBase(String armBase) {
		this.armBase = armBase;
	}

	public String getResMagBase() {
		return resMagBase;
	}

	public void setResMagBase(String resMagBase) {
		this.resMagBase = resMagBase;
	}

	public String getDanoFisBase() {
		return danoFisBase;
	}

	public void setDanoFisBase(String danoFisBase) {
		this.danoFisBase = danoFisBase;
	}

	public String getMovPer() {
		return movPer;
	}

	public void setMovPer(String movPer) {
		this.movPer = movPer;
	}

	public String getRangoPer() {
		return rangoPer;
	}

	public void setRangoPer(String rangoPer) {
		this.rangoPer = rangoPer;
	}

	public String getVelAtaqueBasico() {
		return velAtaqueBasico;
	}

	public void setVelAtaqueBasico(String velAtaqueBasico) {
		this.velAtaqueBasico = velAtaqueBasico;
	}

	public String getVelAtaqueAdicional() {
		return velAtaqueAdicional;
	}

	public void setVelAtaqueAdicional(String velAtaqueAdicional) {
		this.velAtaqueAdicional = velAtaqueAdicional;
	}

	public String getEnlaceOficial() {
		return enlaceOficial;
	}

	public void setEnlaceOficial(String enlaceOficial) {
		this.enlaceOficial = enlaceOficial;
	}

}
