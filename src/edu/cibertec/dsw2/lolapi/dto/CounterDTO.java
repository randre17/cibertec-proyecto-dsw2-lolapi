package edu.cibertec.dsw2.lolapi.dto;

import java.io.Serializable;

public class CounterDTO implements Serializable {

	private long idCounter;
	private long idPer;
	private String ganaLinContra;
	private String pierdeLinContra;
	private String masFuerteContra;
	private String ganaMasContra;
	private String pierdeMasContra;

	public CounterDTO(long idCounter, long idPer, String ganaLinContra, String pierdeLinContra, String masFuerteContra,
			String ganaMasContra, String pierdeMasContra) {
		super();
		this.idCounter = idCounter;
		this.idPer = idPer;
		this.ganaLinContra = ganaLinContra;
		this.pierdeLinContra = pierdeLinContra;
		this.masFuerteContra = masFuerteContra;
		this.ganaMasContra = ganaMasContra;
		this.pierdeMasContra = pierdeMasContra;
	}

	public CounterDTO() {
		super();
	}

	public long getIdCounter() {
		return idCounter;
	}

	public void setIdCounter(long idCounter) {
		this.idCounter = idCounter;
	}

	public long getIdPer() {
		return idPer;
	}

	public void setIdPer(long idPer) {
		this.idPer = idPer;
	}

	public String getGanaLinContra() {
		return ganaLinContra;
	}

	public void setGanaLinContra(String ganaLinContra) {
		this.ganaLinContra = ganaLinContra;
	}

	public String getPierdeLinContra() {
		return pierdeLinContra;
	}

	public void setPierdeLinContra(String pierdeLinContra) {
		this.pierdeLinContra = pierdeLinContra;
	}

	public String getMasFuerteContra() {
		return masFuerteContra;
	}

	public void setMasFuerteContra(String masFuerteContra) {
		this.masFuerteContra = masFuerteContra;
	}

	public String getGanaMasContra() {
		return ganaMasContra;
	}

	public void setGanaMasContra(String ganaMasContra) {
		this.ganaMasContra = ganaMasContra;
	}

	public String getPierdeMasContra() {
		return pierdeMasContra;
	}

	public void setPierdeMasContra(String pierdeMasContra) {
		this.pierdeMasContra = pierdeMasContra;
	}

}
