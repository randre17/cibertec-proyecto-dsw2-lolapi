package edu.cibertec.dsw2.lolapi.dto;

import java.io.Serializable;

public class ProfesionalDTO implements Serializable {

	private long idPro;
	private String nomPro;
	private String paisPro;
	private String fecNacPro;
	private long idRegion;
	private String equipoPro;
	private String rolEquipoPro;
	private String ligaSoloQPro;
	private String idSoloQPro;
	private double prePro;
	private String correoPro;

	public ProfesionalDTO() {
	}

	public ProfesionalDTO(long idPro, String nomPro, String paisPro, String fecNacPro, long idRegion, String equipoPro,
			String rolEquipoPro, String ligaSoloQPro, String idSoloQPro, double prePro, String correoPro) {
		super();
		this.idPro = idPro;
		this.nomPro = nomPro;
		this.paisPro = paisPro;
		this.fecNacPro = fecNacPro;
		this.idRegion = idRegion;
		this.equipoPro = equipoPro;
		this.rolEquipoPro = rolEquipoPro;
		this.ligaSoloQPro = ligaSoloQPro;
		this.idSoloQPro = idSoloQPro;
		this.prePro = prePro;
		this.correoPro = correoPro;
	}

	public long getIdPro() {
		return idPro;
	}

	public void setIdPro(long idPro) {
		this.idPro = idPro;
	}

	public String getNomPro() {
		return nomPro;
	}

	public void setNomPro(String nomPro) {
		this.nomPro = nomPro;
	}

	public String getPaisPro() {
		return paisPro;
	}

	public void setPaisPro(String paisPro) {
		this.paisPro = paisPro;
	}

	public String getFecNacPro() {
		return fecNacPro;
	}

	public void setFecNacPro(String fecNacPro) {
		this.fecNacPro = fecNacPro;
	}

	public long getIdRegion() {
		return idRegion;
	}

	public void setIdRegion(long idRegion) {
		this.idRegion = idRegion;
	}

	public String getEquipoPro() {
		return equipoPro;
	}

	public void setEquipoPro(String equipoPro) {
		this.equipoPro = equipoPro;
	}

	public String getRolEquipoPro() {
		return rolEquipoPro;
	}

	public void setRolEquipoPro(String rolEquipoPro) {
		this.rolEquipoPro = rolEquipoPro;
	}

	public String getLigaSoloQPro() {
		return ligaSoloQPro;
	}

	public void setLigaSoloQPro(String ligaSoloQPro) {
		this.ligaSoloQPro = ligaSoloQPro;
	}

	public String getIdSoloQPro() {
		return idSoloQPro;
	}

	public void setIdSoloQPro(String idSoloQPro) {
		this.idSoloQPro = idSoloQPro;
	}

	public double getPrePro() {
		return prePro;
	}

	public void setPrePro(double prePro) {
		this.prePro = prePro;
	}

	public String getCorreoPro() {
		return correoPro;
	}

	public void setCorreoPro(String correoPro) {
		this.correoPro = correoPro;
	}

}
