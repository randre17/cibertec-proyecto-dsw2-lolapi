package edu.cibertec.dsw2.lolapi.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class RunasRecomendadasDTO implements Serializable {

	private Long idRunaReco;
	private String nomRunaComb;
	private String pickRateRunaReco;
	private String winRateRunaReco;
	private Long idPer;
	private Long idPro;
	private Long idRunaClave;
	private Long idRunaClaveSec;
	private String fragmentoRanura1;
	private String fragmentoRanura2;
	private String fragmentoRanura3;

	public RunasRecomendadasDTO(Long idRunaReco, String nomRunaComb, String pickRateRunaReco, String winRateRunaReco,
			Long idPer, Long idPro, Long idRunaClave, Long idRunaClaveSec, String fragmentoRanura1,
			String fragmentoRanura2, String fragmentoRanura3) {
		super();
		this.idRunaReco = idRunaReco;
		this.nomRunaComb = nomRunaComb;
		this.pickRateRunaReco = pickRateRunaReco;
		this.winRateRunaReco = winRateRunaReco;
		this.idPer = idPer;
		this.idPro = idPro;
		this.idRunaClave = idRunaClave;
		this.idRunaClaveSec = idRunaClaveSec;
		this.fragmentoRanura1 = fragmentoRanura1;
		this.fragmentoRanura2 = fragmentoRanura2;
		this.fragmentoRanura3 = fragmentoRanura3;
	}

	public RunasRecomendadasDTO() {
	}

	public Long getIdRunaReco() {
		return idRunaReco;
	}

	public void setIdRunaReco(Long idRunaReco) {
		this.idRunaReco = idRunaReco;
	}

	public String getNomRunaComb() {
		return nomRunaComb;
	}

	public void setNomRunaComb(String nomRunaComb) {
		this.nomRunaComb = nomRunaComb;
	}

	public String getPickRateRunaReco() {
		return pickRateRunaReco;
	}

	public void setPickRateRunaReco(String pickRateRunaReco) {
		this.pickRateRunaReco = pickRateRunaReco;
	}

	public String getWinRateRunaReco() {
		return winRateRunaReco;
	}

	public void setWinRateRunaReco(String winRateRunaReco) {
		this.winRateRunaReco = winRateRunaReco;
	}

	public Long getIdPer() {
		return idPer;
	}

	public void setIdPer(Long idPer) {
		this.idPer = idPer;
	}

	public Long getIdPro() {
		return idPro;
	}

	public void setIdPro(Long idPro) {
		this.idPro = idPro;
	}

	public Long getIdRunaClave() {
		return idRunaClave;
	}

	public void setIdRunaClave(Long idRunaClave) {
		this.idRunaClave = idRunaClave;
	}

	public Long getIdRunaClaveSec() {
		return idRunaClaveSec;
	}

	public void setIdRunaClaveSec(Long idRunaClaveSec) {
		this.idRunaClaveSec = idRunaClaveSec;
	}

	public String getFragmentoRanura1() {
		return fragmentoRanura1;
	}

	public void setFragmentoRanura1(String fragmentoRanura1) {
		this.fragmentoRanura1 = fragmentoRanura1;
	}

	public String getFragmentoRanura2() {
		return fragmentoRanura2;
	}

	public void setFragmentoRanura2(String fragmentoRanura2) {
		this.fragmentoRanura2 = fragmentoRanura2;
	}

	public String getFragmentoRanura3() {
		return fragmentoRanura3;
	}

	public void setFragmentoRanura3(String fragmentoRanura3) {
		this.fragmentoRanura3 = fragmentoRanura3;
	}

}
