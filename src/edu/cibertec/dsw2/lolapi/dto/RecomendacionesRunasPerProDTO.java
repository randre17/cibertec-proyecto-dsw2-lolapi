package edu.cibertec.dsw2.lolapi.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class RecomendacionesRunasPerProDTO implements Serializable {

	private Long idRunaReco;
	private String nombreRunasContenidas;
	private String tasaSeleccion;
	private String ratioVictorias;
	private String Personaje;
	private PersonajeDTO PersonajeDetalle;
	private String Profesional;
	private ProfesionalDTO ProfesionalDetalle;
	private String runaPrincipal;
	private String runaSecundaria;
	private String fragmentoRanura1;
	private String fragmentoRanura2;
	private String fragmentoRanura3;

	public RecomendacionesRunasPerProDTO(Long idRunaReco, String nombreRunasContenidas, String tasaSeleccion,
			String ratioVictorias, String personaje, PersonajeDTO personajeDetalle, String profesional,
			ProfesionalDTO profesionalDetalle, String runaPrincipal, String runaSecundaria, String fragmentoRanura1,
			String fragmentoRanura2, String fragmentoRanura3) {
		super();
		this.idRunaReco = idRunaReco;
		this.nombreRunasContenidas = nombreRunasContenidas;
		this.tasaSeleccion = tasaSeleccion;
		this.ratioVictorias = ratioVictorias;
		Personaje = personaje;
		PersonajeDetalle = personajeDetalle;
		Profesional = profesional;
		ProfesionalDetalle = profesionalDetalle;
		this.runaPrincipal = runaPrincipal;
		this.runaSecundaria = runaSecundaria;
		this.fragmentoRanura1 = fragmentoRanura1;
		this.fragmentoRanura2 = fragmentoRanura2;
		this.fragmentoRanura3 = fragmentoRanura3;
	}

	public RecomendacionesRunasPerProDTO() {
	}

	public Long getIdRunaReco() {
		return idRunaReco;
	}

	public void setIdRunaReco(Long idRunaReco) {
		this.idRunaReco = idRunaReco;
	}

	public String getNombreRunasContenidas() {
		return nombreRunasContenidas;
	}

	public void setNombreRunasContenidas(String nombreRunasContenidas) {
		this.nombreRunasContenidas = nombreRunasContenidas;
	}

	public String getTasaSeleccion() {
		return tasaSeleccion;
	}

	public void setTasaSeleccion(String tasaSeleccion) {
		this.tasaSeleccion = tasaSeleccion;
	}

	public String getRatioVictorias() {
		return ratioVictorias;
	}

	public void setRatioVictorias(String ratioVictorias) {
		this.ratioVictorias = ratioVictorias;
	}

	public String getPersonaje() {
		return Personaje;
	}

	public void setPersonaje(String personaje) {
		Personaje = personaje;
	}

	public PersonajeDTO getPersonajeDetalle() {
		return PersonajeDetalle;
	}

	public void setPersonajeDetalle(PersonajeDTO personajeDetalle) {
		PersonajeDetalle = personajeDetalle;
	}

	public String getProfesional() {
		return Profesional;
	}

	public void setProfesional(String profesional) {
		Profesional = profesional;
	}

	public ProfesionalDTO getProfesionalDetalle() {
		return ProfesionalDetalle;
	}

	public void setProfesionalDetalle(ProfesionalDTO profesionalDetalle) {
		ProfesionalDetalle = profesionalDetalle;
	}

	public String getRunaPrincipal() {
		return runaPrincipal;
	}

	public void setRunaPrincipal(String runaPrincipal) {
		this.runaPrincipal = runaPrincipal;
	}

	public String getRunaSecundaria() {
		return runaSecundaria;
	}

	public void setRunaSecundaria(String runaSecundaria) {
		this.runaSecundaria = runaSecundaria;
	}

	public String getFragmentoRanura1() {
		return fragmentoRanura1;
	}

	public void setFragmentoRanura1(String fragmentoRanura1) {
		this.fragmentoRanura1 = fragmentoRanura1;
	}

	public String getFragmentoRanura2() {
		return fragmentoRanura2;
	}

	public void setFragmentoRanura2(String fragmentoRanura2) {
		this.fragmentoRanura2 = fragmentoRanura2;
	}

	public String getFragmentoRanura3() {
		return fragmentoRanura3;
	}

	public void setFragmentoRanura3(String fragmentoRanura3) {
		this.fragmentoRanura3 = fragmentoRanura3;
	}

}
