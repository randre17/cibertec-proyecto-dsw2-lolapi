package edu.cibertec.dsw2.lolapi.dto;

import java.io.Serializable;

public class RolPersonajeDTO implements Serializable {
	private long idRolPer;
	private String descRol;

	public RolPersonajeDTO(long idRolPer, String descRol) {
		super();
		this.idRolPer = idRolPer;
		this.descRol = descRol;
	}

	public RolPersonajeDTO() {

	}

	public long getIdRolPer() {
		return idRolPer;
	}

	public void setIdRolPer(long idRolPer) {
		this.idRolPer = idRolPer;
	}

	public String getDescRol() {
		return descRol;
	}

	public void setDescRol(String descRol) {
		this.descRol = descRol;
	}

}
