package edu.cibertec.dsw2.lolapi.bean;

public class RespuestaErrorBean<T> extends RespuestaBean<T> {

	public RespuestaErrorBean(String mensaje) {
		super(mensaje);
	}

	public RespuestaErrorBean(String mensaje, String codigo) {
		super(mensaje, codigo);
	}

}
