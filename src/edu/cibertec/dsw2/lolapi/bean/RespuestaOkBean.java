package edu.cibertec.dsw2.lolapi.bean;

public class RespuestaOkBean<T> extends RespuestaBean<T> {
	
	public RespuestaOkBean(String codigo, String mensaje, T resultado) {
        super(codigo, mensaje, resultado);
    }

    public RespuestaOkBean(String codigo, T resultado) {
        super(codigo, resultado);
    }

}
