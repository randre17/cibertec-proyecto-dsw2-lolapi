package edu.cibertec.dsw2.lolapi.bean;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RespuestaBean<T> implements Serializable {

	@JsonProperty("cod")
	private String codigo;
	@JsonProperty("msg")
	private String mensaje;
	@JsonProperty("resultado")
	private T resultado;

	@JsonProperty("errors")
	@JsonInclude(Include.NON_NULL)
	private List<RespuestaBean<Object>> errors;

	public RespuestaBean() {
	}

	public RespuestaBean(String mensaje) {
		this.mensaje = mensaje;
		codigo = "-";
		resultado = null;
	}

	public RespuestaBean(String mensaje, String codigo) {
		this.mensaje = mensaje;
		this.codigo = codigo;
		resultado = null;
	}

	public RespuestaBean(String codigo, String mensaje, T resultado) {
		this.codigo = codigo;
		this.mensaje = mensaje;
		this.resultado = resultado;
	}

	public RespuestaBean(String codigo, T resultado) {
		this.codigo = codigo;
		this.mensaje = "-";
		this.resultado = resultado;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public T getResultado() {
		return resultado;
	}

	public void setResultado(T resultado) {
		this.resultado = resultado;
	}

	@Override
	public String toString() {
		return "RespuestaBean [codigo=" + codigo + ", mensaje=" + mensaje + ", resultado=" + resultado + "]";
	}

	public List<RespuestaBean<Object>> getErrors() {
		return errors;
	}

	public void setErrors(List<RespuestaBean<Object>> errors) {
		this.errors = errors;
	}
}
