package edu.cibertec.dsw2.lolapi.main;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import io.swagger.jaxrs.config.BeanConfig;

@ApplicationPath("api")
public class LolApiApp extends Application {

	public LolApiApp() {
		BeanConfig beanConfig = new BeanConfig();
		beanConfig.setVersion("1.0.0");
		beanConfig.setTitle("LolApi Servicios");
		beanConfig.setBasePath("/cibertec-proyecto-dsw2-lolapi/api");
		beanConfig.setResourcePackage("edu.cibertec.dsw2.lolapi.rest");
		beanConfig.setScan(true);
		
	}

}
