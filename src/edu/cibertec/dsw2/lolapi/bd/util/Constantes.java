package edu.cibertec.dsw2.lolapi.bd.util;

public class Constantes {
	
	private Constantes() {
		
	}

	public static final String INTEGRADOR_PATH_GET = "/v1/cibertec/dsw2/lolapi/tipoget";
	public static final String INTEGRADOR_PATH_POST = "/v1/cibertec/dsw2/lolapi/tipopost";
	
	public static final String CORRECTO="correcto";
	public static final String ERROR="error";

}
