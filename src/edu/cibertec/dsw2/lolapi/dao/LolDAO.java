package edu.cibertec.dsw2.lolapi.dao;

import java.util.List;

import edu.cibertec.dsw2.lolapi.dto.PersonajeCountersDTO;
import edu.cibertec.dsw2.lolapi.dto.PersonajeDTO;
import edu.cibertec.dsw2.lolapi.dto.ProfesionalCarrilDTO;
import edu.cibertec.dsw2.lolapi.dto.ProfesionalDTO;
import edu.cibertec.dsw2.lolapi.dto.RecomendacionesRunasPerProDTO;
import edu.cibertec.dsw2.lolapi.dto.RegionesDTO;
import edu.cibertec.dsw2.lolapi.dto.ResultadoItemsDTO;
import edu.cibertec.dsw2.lolapi.dto.RunasRecomendadasDTO;

public interface LolDAO {

	public List<RegionesDTO> obtenerRegiones();

	public RegionesDTO insertarRegion(RegionesDTO regionesDTO) throws Exception;

	public List<PersonajeDTO> obtenerPersonajes();

	public ProfesionalDTO insertarProfesional(ProfesionalDTO profesionalDTO) throws Exception;

	public ProfesionalCarrilDTO obtenerProfesionalesPorCarril(String carril);

	public ResultadoItemsDTO obtenerItems();

	public RecomendacionesRunasPerProDTO obtenerRecomendacionesRunasPerPro(String per, String pro);

	public ResultadoItemsDTO obtenerItemsPorLetra(String letra);

	public RunasRecomendadasDTO insertarRunasRecomendadas(RunasRecomendadasDTO runasRecomendadasDTO) throws Exception;

	public PersonajeCountersDTO obtenerCounterPersonajes(String counterPersonaje);

	public void actualizarPersonaje(PersonajeDTO personaje) throws Exception;

}
