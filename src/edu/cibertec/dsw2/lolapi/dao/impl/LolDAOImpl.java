package edu.cibertec.dsw2.lolapi.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import edu.cibertec.dsw2.lolapi.bd.util.UtilBD;
import edu.cibertec.dsw2.lolapi.dao.LolDAO;
import edu.cibertec.dsw2.lolapi.dto.CounterDTO;
import edu.cibertec.dsw2.lolapi.dto.ItemsDTO;
import edu.cibertec.dsw2.lolapi.dto.PersonajeCountersDTO;
import edu.cibertec.dsw2.lolapi.dto.PersonajeDTO;
import edu.cibertec.dsw2.lolapi.dto.ProfesionalCarrilDTO;
import edu.cibertec.dsw2.lolapi.dto.ProfesionalDTO;
import edu.cibertec.dsw2.lolapi.dto.RecomendacionesRunasPerProDTO;
import edu.cibertec.dsw2.lolapi.dto.RegionesDTO;
import edu.cibertec.dsw2.lolapi.dto.ResultadoItemsDTO;
import edu.cibertec.dsw2.lolapi.dto.RunasRecomendadasDTO;

public class LolDAOImpl implements LolDAO {

	public static int indDuplicado;

	@Override
	public List<RegionesDTO> obtenerRegiones() {

		List<RegionesDTO> lstRegiones = new ArrayList<>();
		try {
			Statement statement = UtilBD.getConnection().createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM tb_regiones");
			RegionesDTO regiones = null;
			while (resultSet.next()) {
				regiones = new RegionesDTO();
				regiones.setIdRegion(resultSet.getLong("id_region"));
				regiones.setDesRegion(resultSet.getString("des_region"));
				regiones.setAbvRegion(resultSet.getString("abv_region"));

				lstRegiones.add(regiones);
			}
			resultSet.close();
			statement.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lstRegiones;
	}

	@Override
	public RegionesDTO insertarRegion(RegionesDTO regionesDTO) {

		RegionesDTO reg = new RegionesDTO();
		Connection cnx = null;
		PreparedStatement pst = null;
		try {
			cnx = (Connection) UtilBD.getConnection();
			String sql = "insert into tb_regiones(des_region,abv_region) values (?,?)";
			pst = (PreparedStatement) cnx.prepareStatement(sql);
			pst.setString(1, regionesDTO.getDesRegion());
			pst.setString(2, regionesDTO.getAbvRegion());
			pst.executeUpdate();
			pst.close();
		} catch (SQLIntegrityConstraintViolationException e) {
			// Duplicate entry
			System.out.println("Error de Llave duplicada");
			indDuplicado = 1;
		} catch (SQLException e) {
			// Other SQL Exception
			System.out.println("Error de base de datos");

		} catch (Exception e) {
			System.out.println("Error");
			e.printStackTrace();
		}
		return reg;
	}

	@Override
	public List<PersonajeDTO> obtenerPersonajes() {
		List<PersonajeDTO> lstPersonajes = new ArrayList<>();
		try {
			Statement statement = UtilBD.getConnection().createStatement();
			ResultSet resultSet = statement.executeQuery("select * from tb_personaje");
			PersonajeDTO personaje = null;
			while (resultSet.next()) {
				personaje = new PersonajeDTO();
				personaje.setIdPer(resultSet.getLong("id_per"));
				personaje.setNomPer(resultSet.getString("nom_per"));
				personaje.setIdRolPer(resultSet.getLong("id_rol_per"));
				personaje.setLinPri(resultSet.getString("lin_pri"));
				personaje.setLinSec(resultSet.getString("lin_sec"));
				personaje.setVidaBase(resultSet.getString("vida_base"));
				personaje.setRegVidaBase(resultSet.getString("reg_vida_base"));
				personaje.setManaBase(resultSet.getString("mana_base"));
				personaje.setRegManaBase(resultSet.getString("reg_mana_base"));
				personaje.setArmBase(resultSet.getString("arm_base"));
				personaje.setResMagBase(resultSet.getString("res_mag_base"));
				personaje.setDanoFisBase(resultSet.getString("dano_fis_base"));
				personaje.setMovPer(resultSet.getString("mov_per"));
				personaje.setRangoPer(resultSet.getString("rango_per"));
				personaje.setVelAtaqueBasico(resultSet.getString("vel_ataque_basico"));
				personaje.setVelAtaqueAdicional(resultSet.getString("vel_ataque_adicional"));
				personaje.setEnlaceOficial(resultSet.getString("enlace_oficial"));
				lstPersonajes.add(personaje);
			}
			resultSet.close();
			statement.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lstPersonajes;
	}

	@Override
	public ProfesionalDTO insertarProfesional(ProfesionalDTO profesionalDTO) throws Exception {
		ProfesionalDTO pro = new ProfesionalDTO();
		Connection cnx = null;
		PreparedStatement pst = null;
		try {
			cnx = (Connection) UtilBD.getConnection();
			String sql = "insert into tb_profesional(nom_pro,pais_pro,fec_nac_pro,id_region,equipo_pro,rol_equipo_pro,ligaSoloQ_pro,idSoloQ_pro,pre_pro,correo_pro) values (?,?,?,?,?,?,?,?,?,?)";
			pst = (PreparedStatement) cnx.prepareStatement(sql);
			pst.setString(1, profesionalDTO.getNomPro());
			pst.setString(2, profesionalDTO.getPaisPro());
			pst.setString(3, profesionalDTO.getFecNacPro());
			pst.setLong(4, profesionalDTO.getIdRegion());
			pst.setString(5, profesionalDTO.getEquipoPro());
			pst.setString(6, profesionalDTO.getRolEquipoPro());
			pst.setString(7, profesionalDTO.getLigaSoloQPro());
			pst.setString(8, profesionalDTO.getIdSoloQPro());
			pst.setDouble(9, profesionalDTO.getPrePro());
			pst.setString(10, profesionalDTO.getCorreoPro());
			pst.executeUpdate();
			pst.close();
		} catch (SQLIntegrityConstraintViolationException e) {
			// Duplicate entry
			System.out.println("Error de Llave duplicada");
			indDuplicado = 1;
		} catch (SQLException e) {
			// Other SQL Exception
			System.out.println("Error de base de datos");

		} catch (Exception e) {
			System.out.println("Error");
			e.printStackTrace();
		}
		return pro;
	}

	@Override
	public ProfesionalCarrilDTO obtenerProfesionalesPorCarril(String carril) {

		ProfesionalCarrilDTO profesionalCarrilDTO = new ProfesionalCarrilDTO();
		List<ProfesionalDTO> listaProfesionales = new ArrayList<>();
		int contador = 0;
		Date fechaActual = new Date();
		Connection cnx = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			cnx = (Connection) UtilBD.getConnection();
			String sql = "select * from tb_profesional where rol_equipo_pro like ?";
			pst = (PreparedStatement) cnx.prepareStatement(sql);
			pst.setString(1, "%" + carril + "%");
			pst.executeQuery();
			rs = pst.getResultSet();

			ProfesionalDTO profesional = null;

			while (rs.next()) {
				profesional = new ProfesionalDTO();
				profesional.setIdPro(rs.getLong("id_pro"));
				profesional.setNomPro(rs.getString("nom_pro"));
				profesional.setPaisPro(rs.getString("pais_pro"));
				profesional.setFecNacPro(rs.getString("fec_nac_pro"));
				profesional.setIdRegion(rs.getLong("id_region"));
				profesional.setEquipoPro(rs.getString("equipo_pro"));
				profesional.setRolEquipoPro(rs.getString("rol_equipo_pro"));
				profesional.setLigaSoloQPro(rs.getString("ligaSoloQ_pro"));
				profesional.setIdSoloQPro(rs.getString("idSoloQ_pro"));
				profesional.setPrePro(rs.getDouble("pre_pro"));
				profesional.setCorreoPro(rs.getString("correo_pro"));
				listaProfesionales.add(profesional);
				contador++;
			}
			rs.close();
			pst.close();

			profesionalCarrilDTO.setCantidadProfesionales(contador);
			profesionalCarrilDTO.setCarrilIngresado(carril);
			profesionalCarrilDTO.setFechaBusqueda(fechaActual.toString());
			profesionalCarrilDTO.setListaProfesionales(listaProfesionales);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return profesionalCarrilDTO;
	}

	@Override
	public ResultadoItemsDTO obtenerItems() {
		ResultadoItemsDTO resultadoItemsDTO = new ResultadoItemsDTO();
		List<ItemsDTO> listaItems = new ArrayList<>();
		int contador = 0;
		Date fechaActual = new Date();
		try {
			Statement statement = UtilBD.getConnection().createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM tb_items");
			ItemsDTO item = null;
			while (resultSet.next()) {
				item = new ItemsDTO();
				item.setIdItem(resultSet.getLong("id_item"));
				item.setNomItem(resultSet.getString("nom_item"));
				item.setDesItem(resultSet.getString("des_item"));
				item.setStatsItem(resultSet.getString("stats_item"));
				item.setStatsItem(resultSet.getString("img_item"));
				listaItems.add(item);
				contador++;
			}
			resultSet.close();
			statement.close();

			resultadoItemsDTO.setCantidadItems(contador);
			resultadoItemsDTO.setFechaBusqueda(fechaActual.toString());
			resultadoItemsDTO.setListaItems(listaItems);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultadoItemsDTO;
	}

	@Override
	public RecomendacionesRunasPerProDTO obtenerRecomendacionesRunasPerPro(String per, String pro) {
		RecomendacionesRunasPerProDTO recRunasPerPro = new RecomendacionesRunasPerProDTO();
		PersonajeDTO personajeDTO = new PersonajeDTO();
		ProfesionalDTO profesionalDTO = new ProfesionalDTO();

		Connection cnx = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			cnx = (Connection) UtilBD.getConnection();
			String sql = "select a.id_runa_reco, a.nom_runa_comb, a.pick_rate_runa_reco, a.win_rate_runa_reco,"
					+ "b.nom_per, c.nom_pro, e.nom_runa as 'nom_runa_pri', g.nom_runa as 'nom_runa_sec', "
					+ "a.fragmento_ranura1,a.fragmento_ranura2,a.fragmento_ranura3 " + "from tb_runas_recomendadas a "
					+ "join tb_personaje b on a.id_per = b.id_per " + "join tb_profesional c on a.id_pro = c.id_pro "
					+ "join tb_runa_clave d on a.id_runa_clave = d.id_runa_clave "
					+ "join tb_runa e on d.id_runa = e.id_runa "
					+ "join tb_runa_clave_secundaria f on a.id_runa_clave_sec = f.id_runa_clave_sec "
					+ "join tb_runa g on f.id_runa = g.id_runa " + "where b.nom_per like ? and c.nom_pro like ? ";
			pst = (PreparedStatement) cnx.prepareStatement(sql);
			pst.setString(1, "%" + per + "%");
			pst.setString(2, "%" + pro + "%");
			pst.executeQuery();
			rs = pst.getResultSet();

			while (rs.next()) {
				recRunasPerPro.setIdRunaReco(rs.getLong("a.id_runa_reco"));
				recRunasPerPro.setNombreRunasContenidas(rs.getString("a.nom_runa_comb"));
				recRunasPerPro.setTasaSeleccion(rs.getString("a.pick_rate_runa_reco"));
				recRunasPerPro.setRatioVictorias(rs.getString("a.win_rate_runa_reco"));// -
				recRunasPerPro.setPersonaje(rs.getString("b.nom_per"));
				recRunasPerPro.setProfesional(rs.getString("c.nom_pro"));
				recRunasPerPro.setRunaPrincipal(rs.getString("nom_runa_pri"));
				recRunasPerPro.setRunaSecundaria(rs.getString("nom_runa_sec"));
				recRunasPerPro.setFragmentoRanura1(rs.getString("a.fragmento_ranura1"));
				recRunasPerPro.setFragmentoRanura2(rs.getString("a.fragmento_ranura2"));
				recRunasPerPro.setFragmentoRanura3(rs.getString("a.fragmento_ranura3"));
			}
			rs.close();
			pst.close();

			personajeDTO = this.obtenerPersonajePorNombre(recRunasPerPro.getPersonaje());
			recRunasPerPro.setPersonajeDetalle(personajeDTO);

			profesionalDTO = this.obtenerProfesionalPorNombre(recRunasPerPro.getProfesional());
			recRunasPerPro.setProfesionalDetalle(profesionalDTO);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return recRunasPerPro;
	}

	private PersonajeDTO obtenerPersonajePorNombre(String nombre) {
		PersonajeDTO personaje = new PersonajeDTO();
		Connection cnx = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			cnx = (Connection) UtilBD.getConnection();
			String sql = "select * from tb_personaje where nom_per like ?";
			pst = (PreparedStatement) cnx.prepareStatement(sql);
			pst.setString(1, "%" + nombre + "%");
			pst.executeQuery();
			rs = pst.getResultSet();

			while (rs.next()) {
				personaje.setIdPer(rs.getLong("id_per"));
				personaje.setNomPer(rs.getString("nom_per"));
				personaje.setIdRolPer(rs.getLong("id_rol_per"));
				personaje.setLinPri(rs.getString("lin_pri"));
				personaje.setLinSec(rs.getString("lin_sec"));
				personaje.setVidaBase(rs.getString("vida_base"));
				personaje.setRegVidaBase(rs.getString("reg_vida_base"));
				personaje.setManaBase(rs.getString("mana_base"));
				personaje.setRegManaBase(rs.getString("reg_mana_base"));
				personaje.setArmBase(rs.getString("arm_base"));
				personaje.setResMagBase(rs.getString("res_mag_base"));
				personaje.setDanoFisBase(rs.getString("dano_fis_base"));
				personaje.setMovPer(rs.getString("mov_per"));
				personaje.setRangoPer(rs.getString("rango_per"));
				personaje.setVelAtaqueBasico(rs.getString("vel_ataque_basico"));
				personaje.setVelAtaqueAdicional(rs.getString("vel_ataque_adicional"));
				personaje.setEnlaceOficial(rs.getString("enlace_oficial"));
			}
			rs.close();
			pst.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return personaje;

	}

	private ProfesionalDTO obtenerProfesionalPorNombre(String nombre) {
		ProfesionalDTO profesional = new ProfesionalDTO();
		Connection cnx = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			cnx = (Connection) UtilBD.getConnection();
			String sql = "select * from tb_profesional where nom_pro like ?";
			pst = (PreparedStatement) cnx.prepareStatement(sql);
			pst.setString(1, "%" + nombre + "%");
			pst.executeQuery();
			rs = pst.getResultSet();

			while (rs.next()) {
				profesional.setIdPro(rs.getLong("id_pro"));
				profesional.setNomPro(rs.getString("nom_pro"));
				profesional.setPaisPro(rs.getString("pais_pro"));
				profesional.setFecNacPro(rs.getString("fec_nac_pro"));
				profesional.setIdRegion(rs.getLong("id_region"));
				profesional.setEquipoPro(rs.getString("equipo_pro"));
				profesional.setRolEquipoPro(rs.getString("rol_equipo_pro"));
				profesional.setLigaSoloQPro(rs.getString("ligaSoloQ_pro"));
				profesional.setIdSoloQPro(rs.getString("idSoloQ_pro"));
				profesional.setPrePro(rs.getDouble("pre_pro"));
				profesional.setCorreoPro(rs.getString("correo_pro"));
			}
			rs.close();
			pst.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return profesional;

	}

	@Override
	public ResultadoItemsDTO obtenerItemsPorLetra(String letra) {
		ResultadoItemsDTO resultadoItemsDTO = new ResultadoItemsDTO();
		List<ItemsDTO> listaItems = new ArrayList<>();
		int contador = 0;
		Date fechaActual = new Date();
		Connection cnx = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {

			cnx = (Connection) UtilBD.getConnection();
			String sql = "SELECT * FROM tb_items where nom_item like ?";
			pst = (PreparedStatement) cnx.prepareStatement(sql);
			pst.setString(1, letra + "%");
			pst.executeQuery();
			rs = pst.getResultSet();

			ItemsDTO item = null;
			while (rs.next()) {
				item = new ItemsDTO();
				item.setIdItem(rs.getLong("id_item"));
				item.setNomItem(rs.getString("nom_item"));
				item.setDesItem(rs.getString("des_item"));
				item.setStatsItem(rs.getString("stats_item"));
				item.setStatsItem(rs.getString("img_item"));
				listaItems.add(item);
				contador++;
			}
			rs.close();
			pst.close();

			resultadoItemsDTO.setCantidadItems(contador);
			resultadoItemsDTO.setFechaBusqueda(fechaActual.toString());
			resultadoItemsDTO.setListaItems(listaItems);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultadoItemsDTO;
	}

	@Override
	public RunasRecomendadasDTO insertarRunasRecomendadas(RunasRecomendadasDTO runasRecomendadasDTO) throws Exception {
		RunasRecomendadasDTO runasRecom = new RunasRecomendadasDTO();
		Connection cnx = null;
		PreparedStatement pst = null;
		try {
			cnx = (Connection) UtilBD.getConnection();
			String sql = "insert into tb_runas_recomendadas(nom_runa_comb,pick_rate_runa_reco,win_rate_runa_reco,id_per,id_pro,id_runa_clave,id_runa_clave_sec,fragmento_ranura1,fragmento_ranura2,fragmento_ranura3) values (?,?,?,?,?,?,?,?,?,?)";
			pst = (PreparedStatement) cnx.prepareStatement(sql);
			pst.setString(1, runasRecomendadasDTO.getNomRunaComb());
			pst.setString(2, runasRecomendadasDTO.getPickRateRunaReco());
			pst.setString(3, runasRecomendadasDTO.getWinRateRunaReco());
			pst.setLong(4, runasRecomendadasDTO.getIdPer());
			pst.setLong(5, runasRecomendadasDTO.getIdPro());
			pst.setLong(6, runasRecomendadasDTO.getIdRunaClave());
			pst.setLong(7, runasRecomendadasDTO.getIdRunaClaveSec());
			pst.setString(8, runasRecomendadasDTO.getFragmentoRanura1());
			pst.setString(9, runasRecomendadasDTO.getFragmentoRanura2());
			pst.setString(10, runasRecomendadasDTO.getFragmentoRanura3());
			pst.executeUpdate();
			pst.close();
			
		
		} catch (Exception e) {
			System.out.println("Error");
			e.printStackTrace();
		}
		return runasRecom;
	}
	
	private CounterDTO ObtenerCounter(int idPersonaje) {
		CounterDTO counterDTO = new CounterDTO();
		Connection cnx = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			cnx = (Connection) UtilBD.getConnection();
			String sql = "select * from tb_counter where id_per = ?";
			pst = (PreparedStatement) cnx.prepareStatement(sql);
			pst.setInt(1, idPersonaje);
			pst.executeQuery();
			rs = pst.getResultSet();

			while (rs.next()) {
				counterDTO.setIdCounter(rs.getLong("id_counter"));
				counterDTO.setIdPer(rs.getLong("id_per"));
				counterDTO.setGanaLinContra(rs.getString("gana_lin_contra"));
				counterDTO.setPierdeLinContra(rs.getString("pierde_lin_contra"));
				counterDTO.setMasFuerteContra(rs.getString("mas_fuerte_contra"));
				counterDTO.setGanaMasContra(rs.getString("gana_mas_contra"));
				counterDTO.setPierdeMasContra(rs.getString("pierde_mas_contra"));

			}
			rs.close();
			pst.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return counterDTO;

	}

	@Override
	public PersonajeCountersDTO obtenerCounterPersonajes(String counterPersonaje) {
		
		PersonajeCountersDTO personajeCountersDTO = new PersonajeCountersDTO();
		PersonajeDTO personaje = new PersonajeDTO();
		PersonajeDTO personajeGanaLineacontra = new PersonajeDTO();
		PersonajeDTO personajePierdeLineaContra = new PersonajeDTO();
		PersonajeDTO personajeMasFuerteContra = new PersonajeDTO();
		PersonajeDTO personajeGanaMasContra = new PersonajeDTO();
		PersonajeDTO personajePierdeMasContra = new PersonajeDTO();

		CounterDTO counterDTO = new CounterDTO();
		
		personajeCountersDTO.setPersonajeIngresado(counterPersonaje);
		personaje = this.obtenerPersonajePorNombre(counterPersonaje);
		
		if(personaje==null) {
			counterDTO = null;
			personajeCountersDTO=null;
			return null;
		}else {
			counterDTO = this.ObtenerCounter((int) personaje.getIdPer());
			
			personajeCountersDTO.setPersonajeGanaLineaContra(counterDTO.getGanaLinContra());
			personajeGanaLineacontra = this.obtenerPersonajePorNombre(counterDTO.getGanaLinContra());
			personajeCountersDTO.setDetallePersonajeGanaLineaContra(personajeGanaLineacontra);
			
			personajeCountersDTO.setPersonajePierdeLineaContra(counterDTO.getPierdeLinContra());
			personajePierdeLineaContra = this.obtenerPersonajePorNombre(counterDTO.getPierdeLinContra());
			personajeCountersDTO.setDetallePersonajePierdeLineaContra(personajePierdeLineaContra);
			
			personajeCountersDTO.setPersonajeMasFuerteContra(counterDTO.getMasFuerteContra());
			personajeMasFuerteContra = this.obtenerPersonajePorNombre(counterDTO.getMasFuerteContra());
			personajeCountersDTO.setDetallePersonajeMasFuerteContra(personajeMasFuerteContra);
			
			personajeCountersDTO.setPersonajeGanaMasContra(counterDTO.getGanaMasContra());
			personajeGanaMasContra = this.obtenerPersonajePorNombre(counterDTO.getGanaMasContra());
			personajeCountersDTO.setDetallePersonajeGanaMasContra(personajeGanaMasContra);
			
			personajeCountersDTO.setPersonajePierdeMasContra(counterDTO.getPierdeMasContra());
			personajePierdeMasContra = this.obtenerPersonajePorNombre(counterDTO.getPierdeMasContra());
			personajeCountersDTO.setDetallePersonajePierdeMasContra(personajePierdeMasContra);
			return personajeCountersDTO;
		}
		
	}
	
	@Override
	public void actualizarPersonaje(PersonajeDTO personaje) throws Exception {
		//PersonajeDTO per = new PersonajeDTO();
		Connection cnx = null;
		PreparedStatement pst = null;
		try {
			cnx = (Connection) UtilBD.getConnection();
			String sql = "update tb_personaje set nom_per=?,id_rol_per=?,lin_pri=?,lin_sec=?,"
					+ "vida_base=?,reg_vida_base=?,mana_base=?,reg_mana_base=?,arm_base=?,res_mag_base=?,dano_fis_base=?,mov_per=?,rango_per=?,"
					+ "vel_ataque_basico=?,vel_ataque_adicional=?,enlace_oficial=? where id_per=?";
			pst = (PreparedStatement) cnx.prepareStatement(sql);
			pst.setString(1, personaje.getNomPer());
			pst.setLong(2, personaje.getIdRolPer());
			pst.setString(3, personaje.getLinPri());
			pst.setString(4, personaje.getLinSec());
			pst.setString(5, personaje.getVidaBase());
			pst.setString(6, personaje.getRegVidaBase());
			pst.setString(7, personaje.getManaBase());
			pst.setString(8, personaje.getRegManaBase());
			pst.setString(9, personaje.getArmBase());
			pst.setString(10, personaje.getResMagBase());
			pst.setString(11, personaje.getDanoFisBase());
			pst.setString(12, personaje.getMovPer());
			pst.setString(13, personaje.getRangoPer());
			pst.setString(14, personaje.getVelAtaqueBasico());
			pst.setString(15, personaje.getVelAtaqueAdicional());
			pst.setString(16, personaje.getEnlaceOficial());
			pst.setLong(17, personaje.getIdPer());
			pst.executeUpdate();
			pst.close();
		} catch (Exception e) {
			System.out.println("Error al actualizar");
			e.printStackTrace();
		}
	}

}
