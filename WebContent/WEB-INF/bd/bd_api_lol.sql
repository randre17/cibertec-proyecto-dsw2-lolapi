DROP DATABASE IF EXISTS BD_DSW2_LOL;
CREATE DATABASE IF NOT EXISTS BD_DSW2_LOL;
USE BD_DSW2_LOL;

DROP TABLE IF EXISTS tb_regiones;
CREATE TABLE tb_regiones(
	id_region int auto_increment,
	des_region varchar(50) not null,
    abv_region varchar(50) not null,
    primary key (id_region)
);
ALTER TABLE tb_regiones ADD UNIQUE (des_region);

insert into tb_regiones(des_region,abv_region) values ('Norteamerica','NA');
insert into tb_regiones(des_region,abv_region) values ('Brazil','BR');
insert into tb_regiones(des_region,abv_region) values ('Japon','JP');
insert into tb_regiones(des_region,abv_region) values ('Corea del Sur','KR');
insert into tb_regiones(des_region,abv_region) values ('Latinoamerica Norte','LAN');
insert into tb_regiones(des_region,abv_region) values ('Latinoamerica Sur','LAS');
insert into tb_regiones(des_region,abv_region) values ('Europa','EU');



DROP TABLE IF EXISTS tb_profesional;
CREATE TABLE tb_profesional (
    id_pro INT AUTO_INCREMENT,
    nom_pro VARCHAR(100) NOT NULL,
    pais_pro varchar(50) not null,
    fec_nac_pro varchar(50) not null,
    id_region int not null,
    equipo_pro varchar(50) not null,
	rol_equipo_pro varchar(50) not null,
	ligaSoloQ_pro varchar(100) NOT NULL,
	idSoloQ_pro varchar(100) NOT NULL,
    pre_pro double not null,
    correo_pro varchar(100) not null,
    primary key(id_pro),
    foreign key (id_region) REFERENCES tb_regiones(id_region)
);
ALTER TABLE tb_profesional ADD UNIQUE (nom_pro);
-- ALTER TABLE tb_profesional ADD CONSTRAINT fk_tb_profesional_region FOREIGN KEY (id_region) REFERENCES tb_regiones(id_region);
select * from tb_profesional;


DROP TABLE IF EXISTS tb_rol_personaje;
create table tb_rol_personaje(
id_rol_per INT AUTO_INCREMENT,
desc_rol varchar(40) not null,
primary key(id_rol_per)
);

insert into tb_rol_personaje (desc_rol) values ('Luchador'), ('Tanque'),('Mago'), ('Asesino'), ('Soporte'), ('Tirador');

DROP TABLE IF EXISTS tb_personaje;
create table tb_personaje(
id_per INT AUTO_INCREMENT,
nom_per varchar(30) not null,
id_rol_per INT,
lin_pri varchar(10) not null, /* linea principal*/
lin_sec varchar(10) not null,/* linea secundaia*/
vida_base varchar(7) not null,/*vida inicial*/
reg_vida_base varchar(7) not null,/*reneracion de base*/   /*10*/
mana_base varchar(7) not null,
reg_mana_base varchar(7) not null,
arm_base varchar(7) not null,
res_mag_base varchar(7) not null,
dano_fis_base varchar(7) not null,
mov_per varchar(7) not null,
rango_per varchar(7)not null,
vel_ataque_basico varchar(7)not null,
vel_ataque_adicional varchar(7) not null,
enlace_oficial varchar(500)not null,
primary key (id_per),
foreign key (id_rol_per) references tb_rol_personaje(id_rol_per)
);

insert into tb_personaje(nom_per,id_rol_per, lin_pri, lin_sec,vida_base,reg_vida_base,mana_base,reg_mana_base,arm_base,res_mag_base,dano_fis_base,mov_per,rango_per,vel_ataque_basico,vel_ataque_adicional,enlace_oficial) values 
('LEBLANC',3,'mid lane','top lane','450.00','+ 0.15','200.00','+0.05','30.00','25.00','75.00','80.00','5.50','30.00','45.00','https://lan.leagueoflegends.com/es-mx/champions/leblanc/'),
('ZED',3,'mid lane','top lane','450.00','+ 0.15','200.00','+0.05','30.00','25.00','75.00','80.00','5.50','30.00','45.00','https://lan.leagueoflegends.com/es-mx/champions/zed/'),
('AHRI',3,'mid lane','top lane','450.00','+ 0.15','200.00','+0.05','30.00','25.00','75.00','80.00','5.50','30.00','45.00','https://lan.leagueoflegends.com/es-mx/champions/ahri/'),
('AZIR',3,'mid lane','top lane','450.00','+ 0.15','200.00','+0.05','30.00','25.00','75.00','80.00','5.50','30.00','45.00','https://lan.leagueoflegends.com/es-mx/champions/azir/'),
('RYZE',3,'mid lane','top lane','450.00','+ 0.15','200.00','+0.05','30.00','25.00','75.00','80.00','5.50','30.00','45.00','https://lan.leagueoflegends.com/es-mx/champions/ryze/');

insert into tb_personaje(nom_per,id_rol_per, lin_pri, lin_sec,vida_base,reg_vida_base,mana_base,reg_mana_base,arm_base,res_mag_base,dano_fis_base,mov_per,rango_per,vel_ataque_basico,vel_ataque_adicional,enlace_oficial) values 
('IRELIA',3,'top lane','mid lane','450.00','+ 0.15','200.00','+0.05','30.00','25.00','75.00','80.00','5.50','30.00','45.00','https://lan.leagueoflegends.com/es-mx/champions/leblanc/'),
('LUCIAN',3,'bot lane','mid lane','750.00','+ 0.12','210.00','+0.05','30.00','25.00','75.00','80.00','5.50','30.00','45.00','https://lan.leagueoflegends.com/es-mx/champions/lucian/'),
('YASUO',3,'mid lane','bot lane','550.00','+ 0.05','250.00','+0.05','30.00','25.00','75.00','80.00','5.50','30.00','45.00','https://lan.leagueoflegends.com/es-mx/champions/yasuo/'),
('KATARINA',3,'mid lane','top lane','680.00','+ 0.19','300.00','+0.05','30.00','25.00','75.00','80.00','5.50','30.00','45.00','https://lan.leagueoflegends.com/es-mx/champions/katarina/');

insert into tb_personaje(nom_per,id_rol_per, lin_pri, lin_sec,vida_base,reg_vida_base,mana_base,reg_mana_base,arm_base,res_mag_base,dano_fis_base,mov_per,rango_per,vel_ataque_basico,vel_ataque_adicional,enlace_oficial) values 
('GALIO',3,'top lane','mid lane','450.00','+ 0.15','200.00','+0.05','30.00','25.00','75.00','80.00','5.50','30.00','45.00','https://lan.leagueoflegends.com/es-mx/champions/leblanc/'),
('ILLAOI',3,'top lane','mid lane','450.00','+ 0.15','200.00','+0.05','30.00','25.00','75.00','80.00','5.50','30.00','45.00','https://lan.leagueoflegends.com/es-mx/champions/leblanc/'),
('VELKOZ',3,'mid lane','bot lane','550.00','+ 0.05','250.00','+0.05','30.00','25.00','75.00','80.00','5.50','30.00','45.00','https://lan.leagueoflegends.com/es-mx/champions/yasuo/');
select * from tb_personaje;

DROP TABLE IF EXISTS tb_per_favoritos_pro;
CREATE TABLE tb_per_favoritos (
    id_pro INT not null,
    id_per INT not null,
    
    primary key (id_pro,id_per),
    FOREIGN KEY (id_pro) REFERENCES tb_profesional(id_pro),
    FOREIGN KEY (id_per) REFERENCES tb_personaje(id_per)
);
show tables;
-- HASTA ACA POR AHORA 08-11-2020 20:33
select * from tb_regiones;
select * from tb_rol_personaje;
select * from tb_profesional;

insert into tb_profesional(nom_pro,pais_pro,fec_nac_pro,id_region,equipo_pro,rol_equipo_pro,ligaSoloQ_pro,idSoloQ_pro,pre_pro,correo_pro) 
values ('Gwak Bo-seong (곽보성)','South Korea','March 1, 1999 (age 21)',4,'Gen.G','Mid Laner','Challenger (Retador)','KR: Gen G Bdd,
기찮게하지마,
4wr4DFzQ7T,
412431234,
피넛사냥개1,
개똥벌래z',80.00,'gwak1775@gmail.com');

select * from tb_profesional where rol_equipo_pro like '%mid%';
select * from tb_personaje;

create table tb_items(
id_item INT AUTO_INCREMENT,
nom_item varchar(50)not null,
des_item varchar(250)not null,
stats_item varchar(850)not null,
img_item varchar(250) null,
primary key(id_item)
);

insert into tb_items (nom_item, des_item, stats_item) values ('Aguijón', 'Mayor velocidad de ataque y reducción de enfriamiento', '+35% velocidad de ataque. 
Pasiva ÚNICA: +10% reducción de enfriamiento. Coste: 500 (total 1100)'),
('Amuleto de las hadas', 'Aumenta levemente la regeneración de maná.', '+25% regeneración de maná básica Coste: 125'),
('Ángel de la guarda', 'Revive periódicamente al campeón tras la muerte.', '+45 de daño de ataque
+40 de armadura
Pasiva ÚNICA: Tras sufrir un daño letal, restaura un 50% de la vida básica y un 30% del maná máximo tras 4 s de inmovilidad (300 s de enfriamiento). Coste: 100 (total 2800)'),
('Anillo de Doran', 'Buen objeto inicial para hechiceros.', '+60 de vida
+15 de poder de habilidad
+5 de maná cada 5 s
Pasiva: Los ataques básicos infligen 5 de daño físico adicional a los súbditos al golpear.
Coste: 400'),
('Arco curvo', 'Aumenta en gran medida la velocidad de ataque.', '+25% velocidad de ataque.
Pasiva ÚNICA: Los ataques básicos causan 15 de daño físico adicional al golpear.
Coste: 400 (total 1000)'),
('Armadura de tela', 'Aumenta levemente la armadura.', '+15 armadura
Coste: 300'),
('Armadura de Warmog', 'Otorga una enorme cantidad de vida y regeneración de vida.', '+800 de vida
+200% de regeneración de vida básica
Pasiva ÚNICA: +10% de reducción de enfriamiento
Pasiva ÚNICA: Otorga Corazón de Warmog si tienes al menos 3000 de vida máxima.
Corazón de Warmog: Restaura un 25% de tu vida máxima cada 5 s si no has recibido daño en los últimos 6 s (3 s en el caso de haber recibido daño de súbditos y monstruos).
Coste: 400 (total 2850)'),
('Aro de los Solari de Hierro', 'Puede activarse para proteger a los aliados cercanos del daño.', '+45 de armadura
+75 de resistencia mágica
Activa ÚNICA: Los campeones aliados cercanos reciben un escudo que se desvanece durante 2.5 s y que absorbe hasta 120 (+10 por nivel) (+20% de la vida adicional) de daño (120 s de enfriamiento).
El escudo por nivel utiliza el más alto de los niveles entre el tuyo y el del objetivo.
La relación de escudo progresa con el nivel.
La cantidad de escudo se reduce a 25% si el objetivo ya se ha visto afectado por otro Medallón de los Solari de Hierro en los últimos 20 s.
Coste: 0 (total 2200)'),
('Bacteriófago', 'Los ataques y asesinatos otorgan una pequeña mejora de velocidad.', '+200 vida
+15 daño de ataque
Pasiva ÚNICA, Rabia: Los ataques básicos otorgan 20 de velocidad de movimiento durante 2 s. Los asesinatos otorgan 60. Esta velocidad de movimiento se reduce a la mitad para campeones a distancia.
Coste: 500 (total 1250)'),
('Bailarín espectral', 'Velocidad de movimiento aumentada al atacar enemigos y obtienes un escudo con la vida baja.', '+30% de velocidad de ataque
+25% de probabilidad de impacto crítico
+5% de velocidad de movimiento
Pasiva ÚNICA - Vals espectral: Asestar ataques básicos a un campeón otorga forma fantasmal y un 7% de velocidad de movimiento durante 2 s.
Pasiva ÚNICA - Salvavidas Si recibes daño que hace que tu vida se reduzca por debajo del 30%, obtienes un escudo que absorbe hasta 240 - 600 de daño durante 2 s (90 s de enfriamiento).
Coste: 900 (total 2600)'),
('Baile de la muerte', 'Hace que el daño recibido ahora se reciba más adelante.', '+80 de daño de ataque
+10% de reducción de enfriamiento
Pasiva ÚNICA: Cura un 15% del daño infligido. 33% de efectividad para el daño de área de efecto.
Pasiva ÚNICA: El 30% del daño sufrido se inflige como efecto de sangrado a lo largo de 3 s.
Coste: 625 (total 3500)'),
('Bastón del arcángel', 'Aumenta el poder de habilidad según el maná máximo.', '+50 de poder de habilidad
+650 de maná
+10% de reducción de enfriamiento
Pasiva ÚNICA - Prisa: Este objeto obtiene un 10% más de reducción de enfriamiento.
Pasiva ÚNICA - Asombro: Otorga poder de habilidad equivalente al 1% del maná máximo. Devuelve un 25% del maná gastado.
Pasiva ÚNICA - Carga de maná: Otorga +8 de maná máximo (máximo +750 de maná) por cada gasto de maná (puede suceder hasta 3 veces cada 12 s).
Al alcanzar +750 de maná, se convierte en Abrazo del serafín.
Limitado a 1 objeto que requiere Lágrima de la diosa.
Coste: 1050 (total 3200)'),
('Bastón del arcángel (carga rápida)', 'Aumenta el poder de habilidad según el maná máximo.
+50 de poder de habilidad
+650 de maná
+10% de reducción de enfriamiento', 'Pasiva ÚNICA - Prisa: Este objeto obtiene un 10% más de reducción de enfriamiento.
Pasiva ÚNICA - Asombro: Otorga poder de habilidad equivalente al 1% del maná máximo. Devuelve un 25% del maná gastado.
Pasiva ÚNICA - Carga de maná: Otorga +12 de maná máximo (máximo +750 de maná) por cada gasto de maná (puede suceder hasta 3 veces cada 12 s).
Al alcanzar +750 de maná, se convierte en Abrazo del serafín.
Coste: 1050 (total 3200)'),
('Bastón del Vacío', 'Aumenta el daño mágico.', '+70 poder de habilidad
Pasiva ÚNICA - Disolver: +40% de penetración mágica.
Coste: 1365 (total 2650)'),
('Botas de hechicero', 'Mejora la velocidad de movimiento y el daño mágico.
Limitado a 1 par de botas.', '+18 de penetración mágica
Pasiva ÚNICA - Movimiento mejorado: +45 de velocidad de movimiento.
Coste: 800 (total 1100)'),
('Botas de mercurio', 'Aumenta la velocidad de movimiento y reduce la duración de los efectos incapacitantes. 
Limitado a 1 par de botas.', '+25 de resistencia mágica
Pasiva ÚNICA - Movimiento mejorado: +45 de velocidad de movimiento
Pasiva ÚNICA - Tenacidad: Reduce un 30% la duración de aturdimientos, ralentizaciones, provocaciones, miedos, silencios, cegueras, cambios de forma e inmovilizaciones.
Coste: 350 (total 1100)'),
('Botas de movilidad', 'Mejora en gran medida la velocidad de movimiento cuando no estás en combate. 
Limitado a 1 par de botas.', 'Pasiva ÚNICA - Movimiento mejorado: +25 de velocidad de movimiento. Aumenta hasta +115 de velocidad de movimiento fuera de combate durante 5 s.
Coste: 600 (total 900)'),
('Botas de rapidez', 'Mejora la velocidad de movimiento y reduce el efecto de las ralentizaciones.
Limitado a un 1 par de botas.', 'Pasiva ÚNICA - Movimiento mejorado: +55 de velocidad de movimiento
Pasiva ÚNICA - Resistencia a la ralentización: Los efectos de ralentización del movimiento se reducen un 25%.
Coste: 600 (total 900)'),
('Botas de velocidad', 'Aumenta levemente la velocidad de movimiento.
Limitado a 1 par de botas.', 'Pasiva ÚNICA - Movimiento mejorado: +25 de velocidad de movimiento.
Coste: 300'),
('Botas jonias de la lucidez', 'Aumenta la velocidad de movimiento y la reducción de enfriamiento.
Limitado a 1 par de botas.', 'Pasiva ÚNICA: +10% de reducción de enfriamiento
Pasiva ÚNICA - Movimiento mejorado: +45 de velocidad de movimiento
Pasiva ÚNICA: Reduce un 10% los enfriamientos de los hechizos de invocador.

''Este objeto está dedicado a la victoria de Jonia sobre Noxus en la revancha por las provincias meridionales del 10 de diciembre del 20 CLE''.
Coste: 600 (total 900)'),
('Brazal cristalino', 'Otorga vida y regeneración de vida.', '+200 vida
+50% regeneración de vida básica
Coste: 100 (total 650)'),
('Brazalete de la buscadora', 'Aumenta la armadura y el poder de habilidad.', '+30 armadura
+20 poder de habilidad
Pasiva ÚNICA: Asesinar a una unidad otorga 0.5 de armadura y de poder de habilidad adicionales. Se puede acumular hasta 30 veces.
Coste: 65 (total 1100)'),
('Brillo', 'Otorga una bonificación al siguiente ataque tras lanzar un hechizo.', '+250 maná
+10% reducción de enfriamiento
Pasiva ÚNICA, Hoja Encantada: Tras usar una habilidad, el siguiente ataque básico inflige al impactar daño físico adicional igual al 100% del daño de ataque básico (1.5 s de enfriamiento).
Coste: 700 (total 1050)'),
('Brisa de éter', 'Aumenta el poder de habilidad y la velocidad de movimiento.', '+30 poder de habilidad
Pasiva ÚNICA: +5% velocidad de movimiento
Coste: 415 (total 850)'),
('Calibrador de Sterak', 'Protege frente a grandes descargas de daño.
', '+450 de vida
Pasiva ÚNICA - Fuerza titánica: Obtienes un 50% del daño de ataque básico como daño de ataque adicional.
Pasiva ÚNICA - Salvavidas: Al recibir de 400 a 1800 de daño (según el nivel) en un margen de 5 s, obtienes un escudo equivalente a un 75% de tu vida adicional. Tras 0.75 s, el escudo se va reduciendo a lo largo de 3 s (60 s de enfriamiento).
Furia de Sterak: Cuando Salvavidas se activa, tu tamaño y fuerza aumentan y obtienes +30% de tenacidad durante 8 s.
Coste: 725 (total 3200)'),
('Cáliz de armonía', 'Aumenta la regeneración de vida y de maná.', '+30 de resistencia mágica
+50% de regeneración de maná básica
Pasiva ÚNICA - Armonía: Otorga un porcentaje adicional de regeneración de vida básica equivalente al porcentaje adicional de regeneración de maná básica.
Coste: 100 (total 800)'),
('Cañón de fuego rápido', 'Con el movimiento se acumulan cargas que al liberarse lanzan un ataque de fuego acosador.', '+30% de velocidad de ataque
+25% de probabilidad de impacto crítico
+5% de velocidad de movimiento
Pasiva ÚNICA - Vigor: Al moverte y atacar, podrás lanzar un ataque con vigor.
Pasiva ÚNICA - Francotirador: Tus ataques con vigor infligen 120 de daño mágico adicional. Los ataques con vigor tienen un 35% de alcance adicional (máximo +150 de alcance).
Coste: 500 (total 2600)'),
('Capa de agilidad', 'Aumenta levemente la probabilidad de crítico.', '+25% probabilidad de impacto crítico
Coste: 800'),
('Capa de fuego de forja', 'Causa daño constante a los enemigos cercanos.', '+750 de vida
+100 de armadura
Pasiva ÚNICA - Inmolar: Inflige 25 (+1 por nivel del campeón) de daño mágico por segundo a los enemigos cercanos. Inflige un 50% de daño adicional a súbditos y monstruos.
Coste: 0 (total 2750)'),
('Capa de fuego solar', 'Causa daño constante a los enemigos cercanos.', '+425 vida
+60 armadura
Pasiva ÚNICA - Inmolar: Inflige 25 (+1 por nivel del campeón) de daño mágico cada segundo a los enemigos cercanos. Inflige un 50% de daño adicional a súbditos y monstruos.
Coste: 650 (total 2750)'),
('Capa negatrón', 'Aumenta moderadamente la resistencia mágica.', '+40 resistencia mágica
Coste: 270 (total 720)'),
('Capítulo perdido', 'Recupera maná al subir de nivel.', '+40 de poder de habilidad
+300 de maná
Pasiva ÚNICA - Prisa: +10% de reducción de enfriamiento
Pasiva ÚNICA: Al subir de nivel, se recupera un 20% del maná máximo durante 3 s.
Coste: 80 (total 1300)'),
('Catalizador de eones', 'Gasta maná para recuperar vida.', '+225 de vida
+300 de maná
Pasiva ÚNICA, Eternidad: Se obtiene como maná un 15% del daño sufrido de campeones.
Al gastar maná se recupera un 20% del coste como vida, hasta un máximo de 15 por lanzamiento.
(Los hechizos de activación curan hasta un máximo de 15 cada s).
Coste: 350 (total 1100)'),
('Cetro de cristal de Rylai', 'Las habilidades ralentizan a los enemigos.', '+300 de vida
+90 de poder de habilidad
Pasiva ÚNICA: Los hechizos y habilidades de daño reducen la velocidad de movimiento del enemigo un 20% durante 1 s.
Coste: 915 (total 2600)'),
('Cetro vampírico', 'Los ataques básicos restauran vida.
', '+15 daño de ataque
+10% robo de vida
Coste: 550 (total 900)'),
('Chaleco de cadenas', 'Aumenta en gran medida la armadura.', '+40 armadura
Coste: 500 (total 800)'),
('Chaleco de zarzas', 'Evita que los enemigos usen Robo de vida contra ti.', '+35 de armadura
Pasiva ÚNICA - Espinas: Al recibir un ataque básico, devuelves el daño mágico equivalente a 3 más un 10% de tu armadura adicional, e infliges Heridas graves al atacante durante 3 s.
Coste: 400 (total 1000)'),
('Cimitarra mercurial', 'Puede activarse para quitar todas las debilitaciones de control de adversario y ganar una enorme cantidad de velocidad de moviento', '+50 de daño de ataque
+35 de resistencia mágica
+10% de robo de vida
Activa ÚNICA, Mercurio: Elimina todas las debilitaciones de control de adversario y otorga +50% de velocidad de movimiento durante 1 s (90 s de enfriamiento).
Coste: 325 (total 3400)'),
('Cinturón de gigante', 'Aumenta en gran medida la vida.', '+380 vida
Coste: 600 (total 1000)'),
('Códice diabólico', 'Aumenta el poder de habilidad y la reducción de enfriamiento.', '+35 poder de habilidad
Pasiva ÚNICA: +10% reducción de enfriamiento'),
('Convergencia de Zeke', 'Proporciona bonificaciones a ti y a tus aliados al lanzar la definitiva.', '+60 de armadura
+30 de resistencia mágica
+250 de maná
+10% de reducción de enfriamiento
Activa ÚNICA - Conducto: Te vinculas a un aliado sin un Conducto existente.
Pasiva ÚNICA: Usar tu definitiva cerca de tu aliado invoca una tormenta helada a tu alrededor y prende los ataques básicos de tu aliado durante 10 s. Los enemigos que se encuentren dentro de tu tormenta helada se ven ralentizados un 20%. Además, los ataques de tu aliado queman a sus objetivos con un 30% de daño mágico adicional durante 2 s (45 s de enfriamiento).
Pacto de fuego escarchado: Tu tormenta helada se desata cuando ralentiza a un enemigo con quemaduras, lo que inflige 40 de daño mágico por segundo y ralentiza un 40% en su lugar durante 3 s.
(Los campeones solo pueden estar vinculados por una Convergencia de Zeke a la vez).
Coste: 250 (total 2250)'),
('Coraza del muerto', 'Al moverte vas ganando Impulso que puedes descargar contra tus enemigos.', '+425 de vida
+60 de armadura
Pasiva ÚNICA - Dreadnought: Mientras te mueves vas acumulando Impulso, lo que aumenta tu velocidad de movimiento hasta 60 puntos a las 100 acumulaciones. El impulso decrece al estar bajo los efectos de un aturdimiento, provocación, miedo, polimorfismo o inmovilización.
Pasiva ÚNICA - Golpe aplastante: Los ataques básicos infligen 1 de daño mágico por acumulación de Impulso y gastan todas las acumulaciones. Con el máximo de acumulaciones, si el atacante es cuerpo a cuerpo, también ralentizan al objetivo en un 50% durante 1 s.
''Solo hay un modo de que me arrebates esta armadura...''. - Nombre olvidado
Coste: 1100 (total 2900)'),
('Corazón de hielo', 'Aumenta considerablemente la armadura y ralentiza los ataques básicos del enemigo.', '+100 armadura
+20% reducción de enfriamiento
+400 maná
Aura ÚNICA: Reduce un 15% la velocidad de ataque de los enemigos cercanos.
Coste: 800 (total 2700)'),
('Corona mortal de Rabadon', 'Aumenta enormemente el poder de habilidad.', '+175 de poder de habilidad
Pasiva ÚNICA: Aumenta un 40% el poder de habilidad.
Coste: 0 (total 3600)'),
('Crisol de Mikael', 'Puede activarse para eliminar todos los efectos incapacitantes de un campeón aliado.', '+40 de resistencia mágica
+10% de reducción de enfriamiento
+100% de regeneración de maná básica
Pasiva ÚNICA: +20% de potencia en curaciones y escudos
Pasiva ÚNICA, armonía: otorga un porcentaje de regeneración de vida básica adicional igual al porcentaje de regeneración de maná básico adicional.
Activa ÚNICA: Limpia todos los aturdimientos, inmovilizaciones, provocaciones, miedos, silencios y ralentizaciones en un campeón aliado y le concede inmunidad a ralentizaciones durante 2 s (120 s de enfriamiento).
Eliminar un efecto otorga al aliado un 40% de velocidad de movimiento durante 2 s.
Coste: 500 (total 2100)'),
('Cristal de rubí', 'Aumenta la vida.', '+150 vida
Coste: 400'),
('Cristal de zafiro', 'Aumenta el maná.', '+250 maná
Coste: 350'),
('Cronómetro', 'Puede activarse para hacerte invencible, aunque no podrás realizar acciones.', 'Activa ÚNICA - Estasis: El campeón será invulnerable e inalcanzable durante 2.5 s, pero tampoco podrá moverse, atacar, lanzar hechizos ni usar objetos durante ese tiempo (un solo uso).
Coste: 600'),
('Cronómetro estropeado', 'Se convierte en Cronómetro.', 'Pasiva ÚNICA: Está estropeado, pero, aun así, puede mejorarse.
Después de romper un Cronómetro, el tendero solo te venderá Cronómetros estropeados.
Coste: 600'),
('Cuchilla negra', 'Causar daño físico a campeones enemigos reduce su armadura.', '+400 vida
+40 daño de ataque
+20% reducción de enfriamiento
Pasiva ÚNICA: Al infligir daño físico a un campeón enemigo, lo Acuchilla y reduce su armadura un 4% durante 6 s (se acumula un máximo de 6 veces, hasta llegar a un 24%).
Pasiva ÚNICA, Rabia: Infligir daño físico otorga 20 de velocidad de movimiento durante 2 s. Ayudar a asesinar a campeones enemigos Acuchillados o asesinar a cualquier unidad otorga 60 de velocidad de movimiento durante 2 s. Esta bonificación se reduce a la mitad en el caso de campeones a distancia.
Coste: 950 (total 3000)'),
('Cuchilla obsidiana', 'Causar daño físico a campeones enemigos reduce su armadura.', '+550 de vida
+60 de daño de ataque
+20% de reducción de enfriamiento
Pasiva ÚNICA: Al infligir daño físico a un campeón enemigo, lo acuchilla y reduce su armadura un 4% durante 6 s (se acumula un máximo de 6 veces, hasta llegar a un 24%).
Pasiva ÚNICA - Rabia: Infligir daño físico otorga 20 de velocidad de movimiento durante 2 s. Las asistencias en asesinatos de campeones enemigos acuchillados o los asesinatos de cualquier unidad otorgan 60 de velocidad de movimiento durante 2 s en su lugar. Esta velocidad de movimiento se reduce a la mitad para los campeones a distancia.
Coste: 0 (total 3000)'),
('Daga', 'Aumenta levemente la velocidad de ataque.', '+12% velocidad de ataque
Coste: 300'),
('Daga de hechicero', 'Obtén oro y una mejora al infligir daño a los campeones enemigos.', '+8 de poder de habilidad
+10 de vida
+2 de oro cada 10 s
Pasiva ÚNICA - Tributo: Los ataques y hechizos de daño contra campeones o estructuras otorgan 15 de oro. Esto se puede repetir hasta 3 veces cada 30 s.
MISIÓN: Ganar oro utilizando este objeto hará que se transforme y te otorgará Activa ÚNICA - Vigilancia.
(Recibes cada vez menos oro si matas muchos súbditos).
Limitado a 1 objeto de ganancia de oro o de jungla.
Coste: 400'),
('Daga dentada', 'Aumenta el daño de ataque y la letalidad.', '+25 daño de ataque
Pasiva ÚNICA: +10 de letalidad.
Coste: 400 (total 1100)'),
('Diente de Nashor', 'Aumenta la velocidad de ataque, el poder de habilidad y la reducción de enfriamiento.', '+50% velocidad de ataque
+80 poder de habilidad
Pasiva ÚNICA: +20% reducción de enfriamiento
Pasiva ÚNICA: Los ataques básicos infligen 15 (+15% de poder de habilidad) de daño mágico adicional.
Coste: 1000 (total 3000)'),
('Disfraz encantado', 'Aumenta el daño mágico.', '+35 de poder de habilidad
+200 de vida
Pasiva ÚNICA - Locura: Inflige un 2% más de daño por cada segundo en combate contra campeones (máximo un 10%).
Coste: 665 (total 1500)'),
('Eco de Luden', 'Aumenta el poder de habilidad, el maná y la reducción de enfriamiento.', '+90 de poder de habilidad
+600 de maná
+10% de reducción de enfriamiento
Pasiva ÚNICA - Prisa: Este objeto obtiene un 10% más de reducción de enfriamiento.
Pasiva ÚNICA - Eco: Acumula cargas al moverte o lanzar hechizos. Con 100 cargas, el siguiente hechizo que alcance y dañe a un enemigo consume todas las cargas e inflige 100 (+10% de poder de habilidad) de daño mágico adicional a un máximo de 4 objetivos.
Coste: 1050 (total 3200)'),
('Elixir de brujería', 'Otorga temporalmente poder de habilidad y daño adicional a campeones y torretas.
No se puede comprar antes del nivel 9.'
, 'Pulsar para utilizar. Otorga +50 de poder de habilidad, 15 de regeneración de maná adicional cada 5 s y Brujería durante 3 minutos.
Brujería: Al causar daño a un campeón o una torreta, se inflige 25 de daño verdadero adicional. Este efecto tiene un enfriamiento de 5 s contra campeones, pero contra torretas no tiene enfriamiento.
(Solo se puede tener un Elixir activo)
Coste: 500'),
('Elixir de cólera', 'Otorga temporalmente daño de ataque y te cura cuando infliges daño físico a campeones.
No se puede comprar antes del nivel 9.', 'Pulsar para utilizar. Otorga +30 de daño de ataque y Sed de sangre durante 3 minutos.
Sed de sangre: Se recupera como vida el 15% del daño físico infligido a campeones.
(Solo se puede tener un Elixir activo)
Coste: 500'),
('Elixir de hierro', 'Aumenta temporalmente las defensas y deja un rastro que pueden seguir los aliados.', 'No se puede comprar antes del nivel 9.
Pulsar para utilizar. Otorga +300 de vida, 25% de tenacidad, aumento del tamaño del campeón y Senda del Hierro durante 3 minutos.
Senda del Hierro: Al moverse, se deja detrás una senda que aumenta un 15% la velocidad de movimiento de los campeones aliados.
(Solo se puede tener un Elixir activo)
Coste: 500'),
('Encantamiento: Ecos rúnicos', 'Otorga poder de habilidad y potencia tus hechizos de forma periódica.', '+80 de poder de habilidad
+10% de reducción de enfriamiento
+300 de maná
PASIVA ÚNICA - Eco: Acumula cargas al moverte o lanzar hechizos. A las 100 cargas, el siguiente hechizo que alcance y dañe a un enemigo consume todas las cargas e inflige 60 (+10% de poder de habilidad) de daño mágico adicional a un máximo de 4 objetivos al golpear.
Este efecto inflige un 250% de daño a los monstruos gigantes. Al golpear a un monstruo gigante con este efecto, restauras un 25% del maná que te falte.
Limitado a 1 objeto de ganancia de oro o de jungla.
Coste: 250 (total 2500)'),
('Encantamiento: Ecos rúnicos ', 'Otorga poder de habilidad y potencia tus hechizos de forma periódica.', '+80 de poder de habilidad
+10% de reducción de enfriamiento
+300 de maná
PASIVA ÚNICA - Eco: Acumula cargas al moverte o lanzar hechizos. A las 100 cargas, el siguiente hechizo que alcance y dañe a un enemigo consume todas las cargas e inflige 60 (+10% de poder de habilidad) de daño mágico adicional a un máximo de 4 objetivos al golpear.
Este efecto inflige un 250% de daño a los monstruos gigantes. Al golpear a un monstruo gigante con este efecto, restauras un 25% del maná que te falte.
Limitado a 1 objeto de ganancia de oro o de jungla.
Coste: 250 (total 2500)'),
('Encantamiento: Guerrero', 'Otorga daño de ataque y reducción de enfriamiento', '+60 de daño de ataque
+10% de reducción de enfriamiento
Limitado a 1 objeto de ganancia de oro o de jungla.
Coste: 525 (total 2625)'),
('Encantamiento: Mole de ceniza', 'Otorga vida y Aura de Inmolar.', '+300 de vida
+15% de vida adicional
Pasiva ÚNICA - Inmolar: En combate, inflige 11 (+1 por nivel del campeón) de daño mágico por segundo a los enemigos cercanos. Inflige un 300% de daño adicional a súbditos y monstruos.
Limitado a 1 objeto de ganancia de oro o de jungla.'),
('Encantamiento: Navaja de sangre', 'Aumenta la velocidad de ataque e inflige daño en función de la vida del objetivo.', '+50% de velocidad de ataque
Pasiva ÚNICA: Al golpear, los ataques básicos infligen un 4% de la vida máxima del objetivo como daño físico adicional (75 como máximo contra monstruos y súbditos).
Limitado a 1 objeto de ganancia de oro o de jungla.
Coste: 625 (total 2625)'),
('Ensueño de Shurelya', 'Puede activarse para acelerar a los aliados cercanos.', '+300 de vida
+10% de reducción de enfriamiento
+100% de regeneración de maná básica
+100% de regeneración de vida básica
Pasiva ÚNICA: +10% de curación y poder de escudo.
Activa ÚNICA: Otorga +40% de velocidad de movimiento a tus aliados cercanos y a ti durante 3 s (90 s de enfriamiento).
Coste: 600 (total 2050)'),
('Escudo de Doran', 'Buen objeto defensivo inicial.', '+80 de vida
Pasiva: Restaura 6 de vida cada 5 s.
Pasiva: Los ataques básicos infligen 5 de daño físico adicional contra súbditos al golpear.
Pasiva ÚNICA: Regeneras hasta 40 de vida adicional a lo largo de 8 s después de recibir daño de un campeón enemigo, según el porcentaje de vida que le falte (66% de efectividad para activaciones de campeones a distancia, daño en área o daño periódico).
Coste: 450'),
('Escudo reliquia', 'Obtén oro y una mejora al ejecutar súbditos cerca de los aliados.', '+30 de vida
+5 de poder de habilidad
+2 de oro cada 10 s
Pasiva ÚNICA - Botín de guerra: Cuando estés cerca de un aliado, los ataques básicos cuerpo a cuerpo ejecutan a los súbditos con menos del 50% de la vida máxima (un 30% en el caso de campeones a distancia). Al asesinar a un súbdito, el campeón aliado más cercano recibe el oro correspondiente. (Este efecto se recarga cada 45 s. Máximo 3 cargas).
MISIÓN: Ganar oro utilizando este objeto hará que se transforme y te otorgará Activa ÚNICA - Vigilancia.
(Recibes cada vez menos oro si matas muchos súbditos).
Limitado a 1 objeto de ganancia de oro o de jungla.
Coste: 400'),
('Espada de Doran', 'Buen objeto inicial para atacantes.', '+8 daño de ataque
+80 vida
+3% robo de vida
Coste: 450'),
('Espada del acechador', 'Permite ralentizar campeones con Aplastar', 'Limitado a un objeto de jungla o de ganancia de oro.
+10% de robo de vida contra monstruos
Pasiva ÚNICA - Aplastamiento desalentador: Se puede lanzar Aplastar contra campeones enemigos, lo que les inflige daño verdadero reducido y les roba un 20% de velocidad de movimiento durante 2 s.
Pasiva ÚNICA - Diente/Uña: Los ataques básicos contra monstruos infligen 40 de daño adicional. Al dañar a un monstruo con un hechizo o un ataque, le robas 30 de vida a lo largo de 5 s y le infliges 80 de daño mágico como quemadura. Mientras estés en la jungla o en el río, regeneras hasta 8 de maná por segundo en función del maná que te falte. Este objeto otorga Cazador de monstruos.
Coste: 300 (total 1000)'),
('Espada larga', 'Aumenta levemente el daño de ataque.', '+10 daño de ataque
Coste: 350'),
('Espadón', 'Aumenta en gran medida el daño de ataque.', '+40 daño de ataque
Coste: 1300'),
('Fajín de mercurio', 'Puede activarse para eliminar todas las debilitaciones de control de adversario.', '+30 de resistencia mágica
Activa ÚNICA, Mercurio: Elimina todas las debilitaciones de control de adversario (90 s de enfriamiento).
Coste: 850 (total 1300)'),
('Fauces de Malmortius', 'Otorga daño de ataque adicional cuando tienes poca vida.', '+50 de daño de ataque
+50 de resistencia mágica
+10% de reducción de enfriamiento
Pasiva ÚNICA - Salvavidas: Al recibir daño mágico que reduzca la vida por debajo del 30%, otorga un escudo que absorbe hasta 350 de daño mágico en un margen de 5 s (90 s de enfriamiento).
Agarre a la vida: Cuando Salvavidas se activa, obtienes +20 de daño de ataque, +10% de succión de hechizo y +10% de robo de vida hasta salir de combate.
Coste: 850 (total 3250)'),
('Fervor', 'Leves bonificaciones a la probabilidad de crítico, a la velocidad de movimiento y a la velocidad de ataque.', '+12% velocidad de ataque
+25% probabilidad de crítico
Pasiva ÚNICA: +5% de velocidad de movimiento
Coste: 300 (total 1400)'),
('Filo de la noche', 'Bloquea periódicamente las habilidades enemigas.', '+325 de vida
+50 de daño de ataque
Pasiva ÚNICA: +10 de letalidad.
Pasiva ÚNICA - Escudo de hechizos: Otorga un escudo de hechizos que bloquea la siguiente habilidad enemiga. El escudo se regenera si no sufres daño de campeones enemigos durante 40 s.
Coste: 525 (total 2900)'),
('Filo espectral de Youmuu', 'Puede activarse para aumentar en gran medida la velocidad de movimiento.', '+75 de daño de ataque
+10% de reducción de enfriamiento
Pasiva ÚNICA: +25 de letalidad
Pasiva ÚNICA: +40 de velocidad de movimiento fuera de combate
Activa ÚNICA: Otorga +20% de velocidad de movimiento durante 6 s (45 s de enfriamiento).
Coste: 0 (total 2900)'),
('Filo fundido', 'Mejora enormemente la probabilidad de impacto crítico.', '+110 de daño de ataque
+25% de probabilidad de impacto crítico
Pasiva ÚNICA: Los impactos críticos infligen un 225% de daño en vez de un 200%.
Coste: 0 (total 3400)'),
('Filo infinito', 'Mejora enormemente la probabilidad de impacto crítico.', '+80 de daño de ataque
+25% de probabilidad de impacto crítico
Pasiva ÚNICA: Los impactos críticos infligen un 225% de daño en vez de un 200%.
Coste: 425 (total 3400)'),
('Filoscuro de Draktharr', 'Inflige daño físico adicional al tender emboscadas.', '+60 de daño de ataque
+10% de reducción de enfriamiento
Pasiva ÚNICA: +21 de letalidad.
Pasiva ÚNICA - Acechador nocturno: Después de no ser visto durante al menos 1 s, tu siguiente ataque básico contra un campeón enemigo infligirá 30-150 de daño físico adicional y lo ralentizará un 99% durante 0.25 s. Los ataques básicos a distancia no aplican la ralentización (dura 5 s después de ser visto por un campeón enemigo).
Coste: 700 (total 2900)'),
('Filoscuro de Draktharr', 'Inflige daño físico adicional al tender emboscadas.', '+60 de daño de ataque
+10% de reducción de enfriamiento
Pasiva ÚNICA: +21 de letalidad.
Pasiva ÚNICA - Acechador nocturno: Después de no ser visto durante al menos 1 s, tu siguiente ataque básico contra un campeón enemigo infligirá 30-150 de daño físico adicional y lo ralentizará un 99% durante 0.25 s. Los ataques básicos a distancia no aplican la ralentización (dura 5 s después de ser visto por un campeón enemigo).
Coste: 700 (total 2900)'),
('Final del ingenio', 'Resiste al daño mágico y recupera vida poco a poco.', '+50% de velocidad de ataque
+50 de resistencia mágica
+5% de velocidad de movimiento
Pasiva ÚNICA - Final del ingenio: Inflige 15 - 80 de daño mágico al golpear. Cuando tengas menos de la mitad de vida, te curas un 33% del daño infligido por este efecto, que aumenta a un 100% con campeones cuerpo a cuerpo.
Coste: 880 (total 2900)'),
('Fragmento de Kircheis', 'Velocidad de ataque y un golpe mágico que se puede cargar.', '+15% de velocidad de ataque
Pasiva ÚNICA - Vigor: Al moverte y atacar, podrás lanzar un ataque con vigor.
Pasiva ÚNICA - Descarga: Los ataques con vigor infligen 80 de daño mágico adicional.
Coste: 400 (total 700)'),
('Fuego a discreción', 'Andanada de cañón gana oleadas adicionales.', 'Requiere 500 Serpientes de Plata.
Pasiva ÚNICA: Andanada de cañón aumenta su cadencia de fuego (6 andanadas adicionales por uso de la habilidad).
Coste: 0'),
('Fuerza de trinidad', 'Gran cantidad de daño.', '+250 de vida
+250 de maná
+25 de daño de ataque
+40% de velocidad de ataque
+20% de reducción de enfriamiento
+5% de velocidad de movimiento
Pasiva ÚNICA, Rabia: Los ataques básicos otorgan 20 de velocidad de movimiento durante 2 s. Los asesinatos otorgan 60. Esta bonificación de velocidad de movimiento se reduce a la mitad para campeones a distancia.
Pasiva ÚNICA, Hoja encantada: Tras usar una habilidad, el siguiente ataque básico inflige daño físico adicional igual al 200% del daño de ataque básico (1.5 s de enfriamiento).
Coste: 333 (total 3733)'),
('Fusión de trinidad', 'Gran cantidad de daño', '+350 de vida
+350 de maná
+35 de daño de ataque
+50% de velocidad de ataque
+20% de reducción de enfriamiento
+8% de velocidad de movimiento
Pasiva ÚNICA - Rabia: Los ataques básicos otorgan 20 de velocidad de movimiento durante 2 s. Los asesinatos otorgan 60 de velocidad de movimiento en su lugar. Esta velocidad de movimiento adicional se reduce a la mitad para campeones a distancia.
Pasiva ÚNICA - Hoja encantada: Tras usar una habilidad, el siguiente ataque básico inflige daño físico adicional equivalente a un 200% de daño de ataque básico al golpear (1.5 s de enfriamiento).
Coste: 0 (total 3733)'),
('Gema avivadora', 'Aumenta la vida y la reducción de enfriamiento.', '+200 de vida
Pasiva ÚNICA: +10% de reducción de enfriamiento
Coste: 400 (total 800)'),
('Gloria justiciera', 'Otorga vida, maná y armadura. Actívalo para acelerar hacia el enemigo y ralentizarlo.', '+400 de vida
+300 de maná
+30 de armadura
+100% de regeneración de vida básica
+10% de reducción de enfriamiento
Activa ÚNICA: Otorga un 75% de velocidad de movimiento al moverse hacia unidades o torres enemigas durante 4 s. Cerca de un enemigo (o al cabo de 4 s), la unidad emite una onda de choque que reduce un 75% la velocidad de movimiento de los campeones enemigos durante 2 s (90 s de enfriamiento).
Coste: 1100 (total 2650)'),
('Grebas de berserker', 'Mejora la velocidad de movimiento y la velocidad de ataque.
Limitado a 1 par de botas.', '+35% de velocidad de ataque
Pasiva ÚNICA - Movimiento mejorado: +45 de velocidad de movimiento.
Coste: 500 (total 1100)'),
('Grial impuro de Athene', 'Inflige daño para potenciar tus curaciones y escudos.', '+30 de poder de habilidad
+30 de resistencia mágica
+10% de reducción de enfriamiento
+100% de regeneración de maná básica
Pasiva ÚNICA: Obtienes 35% del daño premitigado infligido a campeones como cargas de sangre, hasta un máximo de 100-250. Curar o ponerle un escudo a otro aliado consume cargas por un valor igual al 100% del valor de la curación o del escudo, y cura al aliado la misma cantidad.
Pasiva ÚNICA - Disonancia: Otorga 5 de poder de habilidad por cada 25% de regeneración de maná básica que tengas. Desactiva Armonía de tus otros objetos.
(La cantidad máxima de cargas de sangre almacenadas depende del nivel. La mejora de la curación se aplica sobre el valor total de curación).
Coste: 400 (total 2100)'),
('Guantelete de hielo', 'Los ataques básicos crean un campo de ralentización tras lanzar un hechizo.', '+65 armadura
+20% reducción de enfriamiento
+500 maná
Pasiva ÚNICA, Hoja Encantada: Tras usar una habilidad, el siguiente ataque básico inflige daño físico adicional equivalente al 100% del daño de ataque básico a los enemigos que estén cerca del objetivo, y crea durante 2 s un campo helado que reduce un 30% la velocidad de movimiento de los rivales (1.5 s de enfriamiento).
El tamaño de la zona aumenta según la armadura adicional.
Coste: 750 (total 2700)'),
('Guardián de control', 'Se usa para desactivar guardianes y trampas invisibles de una zona.
Solo se pueden llevar 2 guardianes de control en el inventario.', 'Pulsar para utilizar: Coloca un guardián que otorga visión de la zona circundante. Este dispositivo también revela las trampas invisibles y revela/deshabilita los guardianes. Los guardianes de control no deshabilitan otros guardianes de control. Las unidades en camuflaje también serán reveladas.
Límite de 1 guardianes de control en el mapa por jugador.
Coste: 75'),
('Guja sombría', 'Otorga detección de trampas y guardianes de forma periódica.', '+50 de daño de ataque
+10% de reducción de enfriamiento
Pasiva ÚNICA: +12 de letalidad.
Pasiva ÚNICA - Apagón: Cuando te detecta un guardián enemigo, se revelan las trampas y se desactivan los guardianes de tu alrededor durante 8 s (45 s de enfriamiento). Los ataques básicos eliminan las trampas reveladas al instante e infligen el triple de daño a guardianes.
Coste: 600 (total 2400)'),
('Hábito del espectro', 'Mejora la defensa y otorga regeneración al recibir daño.', '+250 vida
+25 resistencia mágica
Pasiva ÚNICA: Otorga un 150% de regeneración de vida básica durante un máximo de 10 s tras sufrir daño a manos de un campeón enemigo.
Coste: 350 (total 1200)'),
('Hextech GLP-800', 'Puede activarse para disparar proyectiles helados que ralentizan a los enemigos.', '+80 de poder de habilidad
+600 de maná
+10% de reducción de enfriamiento
Pasiva ÚNICA - Prisa: Este objeto obtiene un 10% más de reducción de enfriamiento.
Activa ÚNICA - Proyectil helado: Dispara una ráfaga de proyectiles helados que explotan en la primera unidad alcanzada e infligen 100-200 (+20% de tu poder de habilidad) de daño mágico a todos los enemigos alcanzados (40 s de enfriamiento que comparten el resto de objetos hextech).
Los enemigos alcanzados sufren una ralentización del 65%, que se va reduciendo a lo largo de 2 s.
Coste: 450 (total 2800)'),
('Hidra titánica', 'Causa daño de área en función de la vida del propietario.', '+450 de vida
+40 de daño de ataque
+100% de regeneración de vida básica
Pasiva ÚNICA - Hender: Los ataques básicos infligen 5 + 1% de tu vida máxima como daño físico adicional al objetivo y 40 + 2.5% de tu vida máxima como daño físico a otros enemigos en un cono al golpear.
Activa ÚNICA - Creciente: El daño de Hender a todos los objetivos aumenta a 40 + 10% de tu vida máxima como daño físico adicional en un cono más grande con tu siguiente ataque básico (20 s de enfriamiento).
(Las pasivas únicas con el mismo nombre no se acumulan).
Coste: 575 (total 3500)'),
('Hidra voraz', 'Los ataques cuerpo a cuerpo afectan a los enemigos cercanos, infligiendo daño y restaurando vida.', '+80 daño de ataque
+100% regeneración de vida básica
+12% robo de vida
Pasiva: El 50% del robo de vida total se aplica al daño infligido por este objeto.
Pasiva ÚNICA - Hender: Los ataques básicos infligen entre un 20% y un 60% del daño de ataque total como daño físico adicional a los enemigos que estén cerca del objetivo impactado (cuanto más cerca estén los enemigos, más daño sufrirán).
Activa ÚNICA - Creciente: Inflige entre un 60% y un 100% del daño de ataque total como daño físico a las unidades enemigas cercanas (cuanto más cerca esté el enemigo, más daño sufrirá) (10 s de enfriamiento).
Coste: 400 (total 3500)'),
('Hija de la muerte', 'Andanada de cañón dispara una megabala de cañón.
Requiere 500 Serpientes de Plata.', 'Pasiva ÚNICA: Andanada de cañón dispara además una megabala de cañón en el centro del bombardeo que inflige un 300% de daño verdadero y ralentiza un 60% a los objetivos durante 1,5 s.
Coste: 0'),
('Hoja carmesí', 'Otorga velocidad de ataque y letalidad al perseguir a campeones enemigos aislados.', '+50 de daño de ataque
+15% de robo de vida
Pasiva ÚNICA: +10 de letalidad.
Pasiva ÚNICA - Sed de sangre: Cuando te encuentres cerca de un campeón enemigo visible o de ninguno, obtienes 8 de letalidad y un 20-80% de velocidad de ataque (en función del nivel), que disminuye a lo largo de 3 s si otro campeón enemigo se acerca demasiado.
Coste: 1100 (total 3100)'),
('Hoja de furia de Guinsoo', 'Aumenta velocidad de ataque, penetración de armadura y penetración mágica', '+25 de daño de ataque
+25 de poder de habilidad
+25% de velocidad de ataque
Pasiva: Los ataques básicos infligen 15 de daño mágico al golpear.
Pasiva ÚNICA - Últimas palabras: Obtienes 15% de penetración de armadura.
Pasiva ÚNICA - Disolver: Obtienes 15% de penetración mágica.
Pasiva ÚNICA: Los ataques básicos otorgan +8% de velocidad de ataque durante 5 s (se acumula hasta 6 veces). Con el máximo de acumulaciones, obtienes Ira de Guinsoo.
Ira de Guinsoo: Cada 3 ataques básicos, los efectos de impacto se activan dos veces.
Con la mitad de acumulaciones, el siguiente ataque de los campeones cuerpo a cuerpo carga Hoja de furia al máximo de acumulaciones.
Coste: 790 (total 3100)'),
('Hoja del rey arruinado', 'Inflige daño según la vida del objetivo; puede robar velocidad de movimiento.', '+40 de daño de ataque
+25% de velocidad de ataque
+12% de robo de vida
Pasiva ÚNICA: Al golpear, los ataques básicos infligen un 8% de la vida actual del objetivo como daño físico adicional.
Activa ÚNICA: Inflige 100 de daño mágico al campeón objetivo y le roba un 25% de la velocidad de movimiento durante 3 s (90 s de enfriamiento).
El daño adicional mínimo infligido es 15.
El daño adicional máximo infligido a monstruos y súbditos es 60.
El robo de vida se aplica al daño adicional infligido.
Coste: 700 (total 3300)'),
('Hombreras de acero', 'Obtén oro y una mejora al ejecutar súbditos cerca de los aliados.', '+30 de vida
+3 de daño de ataque
+2 de oro cada 10 s
Pasiva ÚNICA - Botín de guerra: Cuando estés cerca de un aliado, los ataques básicos cuerpo a cuerpo ejecutan a los súbditos con menos del 50% de la vida máxima (un 30% en el caso de campeones a distancia). Al asesinar a un súbdito, el campeón aliado más cercano recibe el oro correspondiente. (Este efecto se recarga cada 45 s. Máximo 3 cargas).
MISIÓN: Ganar oro utilizando este objeto hará que se transforme y te otorgará Activa ÚNICA - Vigilancia.
(Recibes cada vez menos oro si matas muchos súbditos).
Limitado a 1 objeto de ganancia de oro o de jungla.
Coste: 400'),
('Hoz espectral', 'Obtén oro y una mejora al infligir daño a los campeones enemigos.', '+5 de daño de ataque
+10 de vida
+2 de oro cada 10 s
Pasiva ÚNICA - Tributo: Los ataques y hechizos de daño contra campeones o estructuras otorgan 15 de oro. Esto se puede repetir hasta 3 veces cada 30 s.
MISIÓN: Ganar oro utilizando este objeto hará que se transforme y te otorgará Activa ÚNICA - Vigilancia.
(Recibes cada vez menos oro si matas muchos súbditos).
Limitado a 1 objeto de ganancia de oro o de jungla.
Coste: 400'),
('Huracán de Runaan', 'Los ataques a distancia disparan dos rayos a enemigos cercanos.', '+40% de velocidad de ataque
+25% de probabilidad de impacto crítico
+7% de velocidad de movimiento
Pasiva ÚNICA - Furia de viento: Al usar ataques básicos, se disparan rayos a un máximo de 2 enemigos cercanos al objetivo y cada uno inflige (un 40% del daño de ataque) daño físico. Los proyectiles pueden realizar impactos críticos y provocar efectos de impacto.
Coste: 600 (total 2600)'),
('Ídolo prohibido', 'Aumenta el poder de curaciones y escudos, la regeneración de maná y la reducción de enfriamiento.', '+50% de regeneración de maná básico
Pasiva ÚNICA: +10% de reducción de enfriamiento
Pasiva ÚNICA: +5% de curación y de escudo.
Coste: 550 (total 800)'),
('Impulso de Luden', 'Aumenta el poder de habilidad, el maná y la reducción de enfriamiento.', '+120 de poder de habilidad
+850 de maná
+10% de reducción de enfriamiento
Pasiva ÚNICA - Prisa: Este objeto obtiene un 10% más de reducción de enfriamiento.
Pasiva ÚNICA - Eco: Acumula cargas al moverte o lanzar hechizos. Con 100 cargas, el siguiente hechizo que alcance y dañe a un enemigo consume todas las cargas e inflige 100 (+10% del poder de habilidad) de daño mágico adicional a un máximo de 4 objetivos.
Coste: 0 (total 3200)'),
('Incensario ardiente', 'Los escudos y efectos de curación sobre otras unidades os otorgan velocidad de ataque a ambos y sus ataques infligen daño mágico adicional al golpear.', ''),
('Lágrima de la diosa', 'Aumenta el maná máximo a medida que se gasta maná.', '+250 de maná
Pasiva ÚNICA - Asombro: Devuelve un 10% del maná gastado.
Pasiva ÚNICA - Carga de maná: Otorga 4 de maná máximo al gastar maná (puede suceder hasta 3 veces cada 12 s).
Límite: +750 de maná.
Limitado a 1 objeto que requiere Lágrima de la diosa.
Coste: 375 (total 850)'),
('Lágrima de la diosa (carga rápida)', 'Aumenta el maná máximo a medida que se gasta maná.', '+250 de maná
Pasiva ÚNICA - Asombro: Devuelve un 10% del maná gastado.
Pasiva ÚNICA - Carga de maná: Otorga 6 de maná máximo al gastar maná (puede suceder hasta 3 veces cada 12 s).
Límite: +750 de maná.
Coste: 375 (total 850)'),
('Lanza negra', 'La lanza de Kalista, que la vincula a un Juramentado.', 'Activa: Ofrece un pacto a un aliado durante el resto de la partida, lo que os convierte en Juramentados. Juramentado potencia a ambos campeones mientras estéis cerca el uno del otro.
Coste: 0'),
('Lente del oráculo', 'Durante unos segundos es capaz de detectar las trampas y guardianes invisibles cercanos.
Limitado a 1 talismán.', 'Activa: Escudriña tus alrededores, alerta sobre unidades enemigas hostiles, revela las trampas invisibles y revela/desactiva los guardianes cercanos durante 10 s (de 90 a 60 s de enfriamiento).
Coste: 0'),
('Llamada del verdugo', 'Mayor eficacia contra enemigos que tengan mucha regeneración de vida.', '+15 daño de ataque
Pasiva ÚNICA, Verdugo: El daño físico inflige Heridas Graves a los campeones enemigos durante 3 s.
Coste: 450 (total 800)'),
('Machete del cazador', 'Proporciona daño y robo de vida contra monstruos.', '+10% de robo de vida contra monstruos
Pasiva ÚNICA - Uña: Al golpear, los ataques básicos infligen 35 de daño adicional contra monstruos. Este objeto otorga Cazador de monstruos.
Limitado a 1 objeto de ganancia de oro o de jungla.
Coste: 350'),
('Malla de espinas', 'Devuelve el daño sufrido de ataques básicos como daño mágico.', '+250 de vida
+80 de armadura
Pasiva ÚNICA - Espinas: Al recibir un ataque básico, devuelves el daño mágico equivalente al 10% de tu armadura adicional más 25 e infliges Heridas graves al atacante durante 3 s.
Pasiva ÚNICA - Acero frío: Cuando recibes ataques básicos, reduces un 15% la velocidad de ataque del atacante durante 1 s.
Coste: 500 (total 2900)'),
('Malla del guardián', 'Reduce la velocidad de ataque de los campeones enemigos al recibir ataques básicos.', '+40 armadura
Pasiva ÚNICA , Acero frío: Si te alcanza un ataque básico, reduces un 15% la velocidad de ataque del atacante durante 1 s.
Coste: 400 (total 1000)'),
('Manamune', 'Aumenta el daño de ataque según el maná máximo.', '+35 de daño de ataque
+250 de maná
Pasiva ÚNICA - Asombro: Otorga daño de ataque adicional equivalente al 2% del maná máximo. Devuelve un 15% del maná gastado.
Pasiva ÚNICA - Carga de maná: Otorga +5 de maná máximo (máximo +750 de maná) por cada ataque básico o gasto de maná (puede suceder hasta 3 veces cada 12 s).
Al alcanzar +750 de maná, se convierte en Muramana.
Limitado a 1 objeto que requiere Lágrima de la diosa.
Coste: 675 (total 2400)'),
('Manamune (carga rápida)', 'Aumenta el daño de ataque según el maná máximo.', '+35 de daño de ataque
+250 de maná
Pasiva ÚNICA - Asombro: Otorga daño de ataque adicional equivalente al 2% del maná máximo. Devuelve un 15% del maná gastado.
Pasiva ÚNICA - Carga de maná: Otorga +6 de maná máximo (máximo +750 de maná) por cada ataque básico o gasto de maná (puede suceder hasta 3 veces cada 12 s).
Al alcanzar +750 de maná, se convierte en Muramana.
Coste: 675 (total 2400)'),
('Manto de anulación de magia', 'Aumenta levemente la resistencia mágica.', '+25 resistencia mágica
Coste: 450'),
('Martillo de guerra de Caulfield', 'Daño de ataque y reducción de enfriamiento.', '+25 daño de ataque
Pasiva ÚNICA: +10% de reducción de enfriamiento
Coste: 400 (total 1100)'),
('Máscara abisal', 'Los enemigos cercanos reciben más daño mágico.', '+350 de vida
+300 de maná
+55 de resistencia mágica
+10% de reducción de enfriamiento
Pasiva ÚNICA - Eternidad: Se recupera un 15% del daño infligido por campeones en forma de maná. Al gastar maná, se recupera un 20% del coste como vida, hasta un máximo de 25 por lanzamiento.
Aura ÚNICA: Los campeones enemigos cercanos reciben un 15% más de daño mágico.
Coste: 1180 (total 3000)'),
('Máscara infernal', 'Los enemigos cercanos reciben más daño mágico.', '+750 de vida
+300 de maná
+100 de resistencia mágica
+10% de reducción de enfriamiento
Pasiva ÚNICA - Eternidad: Se recupera un 15% del daño infligido por campeones en forma de maná. Al gastar maná, se recupera un 20% del coste como vida, hasta un máximo de 25 por lanzamiento.
Aura ÚNICA: Los campeones enemigos cercanos reciben un 15% más de daño mágico.
Coste: 0 (total 3000)'),
('Mazo helado', 'Los ataques básicos ralentizan a los enemigos.', '+700 vida
+30 daño de ataque
Pasiva ÚNICA, Gélido: Los ataques básicos ralentizan la velocidad de movimiento del objetivo durante 1.5 s al impactar (40% de ralentización en los ataques cuerpo a cuerpo, 20% de ralentización en los ataques a distancia).
Coste: 900 (total 3100)'),
('Medallón de los Solari de Hierro', 'Puede activarse para proteger a los aliados cercanos del daño.', '+30 de armadura
+60 de resistencia mágica
Activa ÚNICA: Los campeones aliados cercanos reciben un escudo que se desvanece durante 2.5 s y que absorbe hasta 120 (+10 por nivel) (+20% de la vida adicional) de daño (120 s de enfriamiento).
El escudo por nivel utiliza el más alto de los niveles entre el tuyo y el del objetivo.
La relación de escudo progresa con el nivel.
La cantidad de escudo se reduce a 25% si el objetivo ya se ha visto afectado por otro Medallón de los Solari de Hierro en los últimos 20 s.
Coste: 650 (total 2200)'),
('Morellonomicón', 'Aumenta el daño mágico.', '+70 de poder de habilidad
+300 de vida
Pasiva ÚNICA - Toque de la muerte: +15 de penetración mágica
Pasiva ÚNICA - Golpe maldito: El daño mágico infligido a campeones les aplica Heridas graves durante 3 s.
Coste: 550 (total 3000)'),
('Navaja de asalto', 'Potencia enormemente otros efectos con Vigor.', '+55 de daño de ataque
+25% de probabilidad de impacto crítico
+15% de velocidad de ataque
Pasiva ÚNICA - Vigor: Al moverte y atacar, podrás lanzar un ataque con vigor.
Pasiva ÚNICA - Paralizar: Tus ataques con vigor infligen 120 de daño mágico adicional. Los ataques con vigor aplican una ralentización del 75% durante 0.5 s.
Coste: 400 (total 3200)'),
('Núcleo hex perfecto', 'Permite a Viktor mejorar una habilidad de su elección.', '+10 poder de habilidad por nivel
+25 maná por nivel
Pasiva ÚNICA, Evolución gloriosa: Viktor ha alcanzado la cima de su poder y puede mejorar Tormenta del caos además de sus otras habilidades.
Coste: 850 (total 3000)'),
('Núcleo hex tipo 1', 'Permite a Viktor mejorar una habilidad de su elección.', '+3 poder de habilidad por nivel
+15 maná por nivel
Pasiva ÚNICA, Progreso: Viktor puede mejorar una de sus habilidades.
Coste: 1150'),
('Núcleo hex tipo 2', 'Permite a Viktor mejorar una habilidad de su elección.', '+6 poder de habilidad por nivel
+20 maná por nivel
Pasiva ÚNICA, Progreso: Viktor puede mejorar una de sus habilidades.
Coste: 1000 (total 2150)'),
('Orbe del olvido', 'Aumenta el daño mágico.', '+20 de poder de habilidad
+200 de vida
Pasiva ÚNICA - Toque de la muerte: +15 de penetración mágica.
Coste: 765 (total 1600)'),
('Paradoja de Zhonya', 'Puede activarse para hacerte invencible, aunque no podrás realizar acciones.', '+100 de poder de habilidad
+60 de armadura
+10% de reducción de enfriamiento
Activa ÚNICA - Estasis: El campeón será invulnerable e inalcanzable durante 2.5 s, pero tampoco podrá moverse, atacar, lanzar hechizos o usar objetos durante ese tiempo (120 s de enfriamiento).
Coste: 0 (total 2900)'),
('Perdición del liche', 'Otorga una bonificación al siguiente ataque tras lanzar un hechizo.', '+80 poder de habilidad
+7% velocidad de movimiento
+10% reducción de enfriamiento
+250 maná
Pasiva ÚNICA, Hoja Encantada: Tras usar una habilidad, el siguiente ataque básico inflige un 75% del daño de ataque básico (+50% de poder de habilidad) como daño mágico adicional al impactar (1.5 s de enfriamiento).
Coste: 450 (total 3200)'),
('Perdición del liche', 'Otorga una bonificación al siguiente ataque tras lanzar un hechizo.', '+80 poder de habilidad
+7% velocidad de movimiento
+10% reducción de enfriamiento
+250 maná
Pasiva ÚNICA, Hoja Encantada: Tras usar una habilidad, el siguiente ataque básico inflige un 75% del daño de ataque básico (+50% de poder de habilidad) como daño mágico adicional al impactar (1.5 s de enfriamiento).
Coste: 450 (total 3200)'),
('Perla de rejuvenecimiento', 'Aumenta levemente la regeneración de vida.', '+50% regeneración de vida básica
Coste: 150'),
('Pico', 'Aumenta moderadamente el daño de ataque.', '+25 daño de ataque
Coste: 875'),
('Poción de corrupción', 'Restaura vida y maná a lo largo del tiempo y aumenta el poder de combate. Se rellena en la tienda.
Limitado a un tipo de poción de curación.', 'Activa ÚNICA: Consume una carga para recuperar 125 de vida y 75 de maná durante 12 s. Otorga Toque de corrupción durante ese tiempo. Contiene un máximo de 3 cargas y se rellena al visitar la tienda.
Toque de corrupción: Los ataques y hechizos de daño queman a los campeones enemigos e infligen 15 de daño mágico durante 3 s (50% de daño en el caso de hechizos de efecto en área o de daño prolongado. El daño aumenta según el nivel del campeón).
Poción de corrupción se puede usar incluso con la vida y el maná completos.
Coste: 350 (total 500)'),
('Poción de vida', 'Al usarse restaura vida a lo largo del tiempo.
Limitado a 5 a la vez. Limitado a 1 tipo de poción de curación.', 'Pulsar para utilizar. Restaura 150 de vida a lo largo de 15 s.
Coste: 50'),
('Poción reutilizable', 'Restaura vida a lo largo del tiempo y se rellena en la tienda.
Limitado a 1 tipo de poción de curación.', 'Activa ÚNICA: Consume una carga para restaurar 125 de vida a lo largo de 12 s. Acumula un máximo de 2 cargas y se rellena al visitar la tienda.
Coste: 150'),
('Poder del rey arruinado', 'Inflige daño según la vida del objetivo; puede robar velocidad de movimiento.', '+55 de daño de ataque
+40% de velocidad de ataque
+12% de robo de vida
Pasiva ÚNICA: Al golpear, los ataques básicos infligen un 8% de la vida actual del objetivo como daño físico adicional.
Activa ÚNICA: Inflige 100 de daño mágico al campeón objetivo y le roba un 25% de la velocidad de movimiento durante 3 s (90 s de enfriamiento).
El daño adicional mínimo infligido es 15.
El daño adicional máximo infligido a monstruos y súbditos es 60.
El robo de vida se aplica al daño adicional infligido.
Coste: 0 (total 3300)'),
('Preferencia', 'Proporciona daño y robo de vida al golpear. Matar súbditos otorga oro adicional.', '+7 daño de ataque
+3 vida al golpear
Pasiva ÚNICA: Matar a un súbdito de calle otorga 1 de oro adicional. Matar a 100 súbditos de calle otorga además 350 de oro adicional al momento y desactiva esta pasiva.
Coste: 450'),
('Presagio de Randuin', 'Aumenta notablemente las defensas; puede activarse para ralentizar a los enemigos cercanos.', '+400 de vida
+70 de armadura
Pasiva ÚNICA: -20% de daño recibido de golpes críticos de ataques básicos.
Pasiva ÚNICA - Acero frío: Si te alcanza un ataque básico, reduces un 15% la velocidad de ataque del atacante durante 1 s.
Activa ÚNICA: Reduce un 55% la velocidad de movimiento de las unidades enemigas cercanas durante 2 s (60 s de enfriamiento).
Coste: 900 (total 2900)'),
('Promesa de caballero', 'Asóciate con un aliado para protegeros el uno al otro.', '+250 de vida
+40 de armadura
+10% de reducción de enfriamiento
Activa ÚNICA: Escoge a un campeón aliado como tu compañero (90 s de enfriamiento).
Pasiva ÚNICA: Si tu compañero está cerca, obtienes +20 de armadura adicional y +15% de velocidad de movimiento al moverte hacia él.
Pasiva ÚNICA: Si tu compañero está cerca, te curas un 12% del daño que tu compañero inflige a los campeones y rediriges hacia ti un 12% del daño que tu compañero recibe de los campeones como daño verdadero (la curación y la redirección de daño se reducen un 50% si eres un campeón a distancia).
(Los campeones solo pueden estar vinculados por una Promesa de caballero a la vez).
Coste: 600 (total 2200)'),
('Protección de la legión', 'Otorga armadura y resistencia mágica.', '+30 de armadura
+30 de resistencia mágica
Coste: 350 (total 1100)'),
('Protector pétreo de gárgola', 'Aumenta considerablemente la defensa cerca de varios enemigos.', '+40 de armadura
+40 de resistencia mágica
Pasiva ÚNICA - Piel de piedra: Si hay más de 3 campeones enemigos cerca, otorga 40 de armadura y resistencia mágica adicionales.
Activa ÚNICA - Metalización: Aumenta la vida un 40% y el tamaño del campeón, pero reduce el daño infligido un 60% durante 4 s (90 s de enfriamiento). Si Piel de piedra está activa, el aumento de vida es de un 100%.
Coste: 380 (total 2500)'),
('Protocinturón hextech-01', 'Puede activarse para desplazarse hacia delante y desencadenar una explosión ardiente.', '+300 de vida
+60 de poder de habilidad
+10% de reducción de enfriamiento
Activa ÚNICA - Proyectil ígneo: Te desplazas hacia delante y lanzas un anillo de proyectiles ígneos que infligen 75-150 (+25% de tu poder de habilidad) como daño mágico (40 s de enfriamiento que comparten el resto de objetos hextech).
Los campeones y monstruos golpeados por varios proyectiles ígneos sufren un 10% de daño por cada proyectil adicional.
(El deslizamiento no permite atravesar obstáculos del terreno).
Coste: 650 (total 2500)'),
('Puñal de Statikk', 'El movimiento genera cargas que liberan rayos con el ataque básico.', '+40% de velocidad de ataque
+25% de probabilidad de impacto crítico
+5% de velocidad de movimiento
Pasiva ÚNICA - Vigor: Al moverte y atacar, podrás lanzar un ataque con vigor.
Pasiva ÚNICA - Electrochoque: Los ataques con vigor infligen 120 de daño mágico adicional. Los efectos con vigor se propagan a 7 objetivos.
Coste: 500 (total 2600)'),
('Puño de Jaurim', 'Daño de ataque y acumulaciones de vida al asesinar a una unidad.', '+15 daño de ataque
+200 vida
Pasiva ÚNICA: Asesinar a una unidad otorga 5 de vida máxima. Se puede acumular hasta 20 veces.
Coste: 450 (total 1200)'),
('Puño helado', 'Los ataques básicos crean un campo de ralentización tras lanzar un hechizo.', '+125 de armadura
+20% de reducción de enfriamiento
+700 de maná
Pasiva ÚNICA, Hoja Encantada: Tras usar una habilidad, el siguiente ataque básico inflige daño físico adicional equivalente al 100% del daño de ataque básico en un área y crea durante 2 s un campo helado que reduce un 30% la velocidad de movimiento (1.5 s de enfriamiento).
El tamaño de la zona aumenta según la armadura adicional.
Coste: 0 (total 2700)'),
('Recordatorio letal', 'Eficaz contra enemigos que tengan mucha recuperación de vida y armadura.', '+45 de daño de ataque
Pasiva ÚNICA, Últimas palabras: +25% de penetración de armadura
Pasiva ÚNICA, Verdugo: El daño físico inflige Heridas Graves a los campeones enemigos durante 5 s.
Coste: 550 (total 2800)'),
('Recuerdos de lord Dominik', 'Mayor eficacia contra enemigos que tengan mucha vida y armadura.', '+45 de daño de ataque
Pasiva ÚNICA, Últimas palabras: +35% de penetración de armadura.
Coste: 475 (total 2800)'),
('Redención', 'Puede activarse para curar a los aliados e infligir daño a los enemigos en el área.', '+200 de vida
+50% de regeneración de vida básica
+150% de regeneración de maná básica
+10% de reducción de enfriamiento
Pasiva ÚNICA: +10% de poder de curaciones y escudos.
Activa ÚNICA: Selecciona como objetivo un área a una distancia máxima de 5500. Después de 2,5 s, invoca un rayo de luz que cura a los aliados 10 (+20 por nivel del objetivo) de vida, quema a los campeones enemigos un 10% de su vida máxima como daño verdadero e inflige 250 de daño verdadero a los súbditos enemigos (120 s de enfriamiento). El poder de curaciones y escudos es 3 veces más efectivo para la curación de Redención.
Se puede usar estando muerto.
Tiene la mitad del efecto si el objetivo ha recibido los efectos de otra Redención hace poco.
Coste: 650 (total 2100)'),
('Reloj de arena de Zhonya', 'Puede activarse para hacerte invencible, aunque no podrás realizar acciones.', '+75 de poder de habilidad
+45 de armadura
+10% de reducción de enfriamiento
Activa ÚNICA, Estasis: El campeón será invulnerable e inalcanzable durante 2.5 s, pero tampoco podrá moverse, atacar, lanzar hechizos o usar objetos durante ese tiempo (120 s de enfriamiento).
Coste: 300 (total 2900)'),
('Rescoldo de Bami', 'Otorga vida y Aura de Inmolar', '+200 vida
Pasiva ÚNICA, Inmolar: Inflige 5 (+1 por nivel del personaje) de daño mágico por segundo a los enemigos cercanos. Inflige un 100% de daño adicional a súbditos y monstruos.
Coste: 500 (total 900)'),
('Revólver hextech', 'Aumenta el poder de habilidad. Inflige daño mágico adicional con ataques periódicos.', '+40 de poder de habilidad 
Pasiva ÚNICA - Proyectil mágico: Infligir daño a un campeón enemigo con ataques básicos causa 50-125 de daño mágico adicional (40 s de enfriamiento que comparten el resto de objetos hextech).
El enfriamiento de Proyectil mágico se reduce por la reducción de enfriamiento del objeto activo.
(El daño aumenta según el nivel. Los efectos hextech pueden activar efectos de hechizo de otros objetos).
Coste: 180 (total 1050)'),
('Robaalmas de Mejai', 'Concede poder de habilidad por asesinatos y ayudas.', '+20 de poder de habilidad
+200 de maná
Pasiva ÚNICA - Pavor: Otorga +5 de poder de habilidad por acumulación de Gloria. Otorga 10% de velocidad de movimiento si tienes al menos 10 acumulaciones.
Pasiva ÚNICA - Todo o nada: Otorga 4 de Gloria por asesinato de campeón o 2 de Gloria por ayuda, hasta un total de 25 acumulaciones de Gloria. Se pierden 10 acumulaciones de Gloria al morir.
Coste: 1050 (total 1400)'),
('Rostro espiritual', 'Aumenta la vida y los efectos de curación.', '+450 vida
+55 de resistencia mágica
+100% de regeneración de vida básica
+10% de reducción de enfriamiento
Pasiva ÚNICA: Aumenta un 30% toda la curación recibida.
Coste: 800 (total 2800)'),
('Sable de Aguas Estancadas', 'Puede activarse para causar daño mágico y ralentizar al campeón objetivo.', '+25 daño de ataque
+10% robo de vida
Activa ÚNICA: Inflige 100 de daño mágico y reduce un 25% la velocidad de movimiento del campeón objetivo durante 2 s (90 s de enfriamiento).
Coste: 350 (total 1600)'),
('Sable de escaramuza', 'Te permite usar Aplastar para marcar campeones, con lo que recibes poderes de combate contra ellos.
Limitado a un objeto de jungla o de ganancia de oro.', '+10% de robo de vida contra monstruos
Pasiva ÚNICA - Aplastamiento desafiante: Se puede lanzar Aplastar contra campeones enemigos para marcarlos durante 4 s. Mientras estén marcados, tus ataques básicos le infligen daño verdadero adicional durante 2.5 s y recibes un 20% menos de daño.
Los ataques básicos contra monstruos infligen 40 de daño adicional. Al dañar a un monstruo con un hechizo o un ataque, le robas 30 de vida a lo largo de 5 s y le infliges 80 de daño mágico como quemadura. Mientras estés en la jungla o en el río, regeneras hasta 8 de maná por segundo en función del maná que te falte. Este objeto otorga Cazador de monstruos.
Coste: 300 (total 1000)'),
('Sable-pistola hextech', 'Aumenta el daño de ataque y el poder de habilidad; se puede activar para ralentizar a un objetivo.', '+40 de daño de ataque
+80 de poder de habilidad
Pasiva ÚNICA: Cura un 15% del daño infligido. La efectividad es del 33% para los ataques con efecto en área.
Activa ÚNICA - Proyectil fugaz: Inflige 175-253 (+30% de poder de habilidad) de daño mágico y ralentiza un 40% la velocidad de movimiento del campeón objetivo durante 2 s (40 s de enfriamiento que comparten el resto de objetos hextech).
Coste: 750 (total 3400)'),
('Salvación', 'Puede activarse para curar a los aliados e infligir daño a los enemigos en el área.', '+300 de vida
+150% de regeneración de vida básica
+200% de regeneración de maná básica
+10% de reducción de enfriamiento
Pasiva ÚNICA: +10% de poder de curaciones y escudos.
Activa ÚNICA: Selecciona como objetivo un área a una distancia máxima de 5500. Después de 2,5 s, invoca un rayo de luz que cura a los aliados 10 (+20 por nivel del objetivo) de vida, quema a los campeones enemigos un 10% de su vida máxima como daño verdadero e inflige 250 de daño verdadero a los súbditos enemigos (120 s de enfriamiento). El poder de curaciones y escudos es 3 veces más efectivo para la curación de Salvación.
Se puede usar estando muerto.
Tiene la mitad del efecto si el objetivo ha recibido los efectos de Redención hace poco.
Coste: 0 (total 2100)'),
('Sanguinaria', 'Aumenta el daño de ataque y otorga robo de vida. Además permite acumular vida por encima del máximo.', '+80 daño de ataque
Pasiva ÚNICA: +20% de robo de vida
Pasiva ÚNICA: Los ataques básicos pueden curarte por encima de tu nivel de vida normal. La vida adicional se acumula como un escudo que puede bloquear 50-350 de daño, en función del nivel de campeón.
Este escudo va decreciendo lentamente si no has infligido o recibido daño en los últimos 25 s.
Coste: 950 (total 3500)'),
('Segador de esencia', 'Otorga probabilidad de impacto crítico, reducción de enfriamiento, daño de ataque y recupera maná al golpear.', '+70 de daño de ataque
+25% de probabilidad de impacto crítico
Pasiva ÚNICA: +20% de reducción de enfriamiento
Pasiva ÚNICA: Los ataques básicos recuperan un 1.5% del maná que te falte.
Coste: 100 (total 3300)'),
('Sello oscuro', 'Proporciona poder de habilidad y maná. Aumenta su poder a medida que matas enemigos.', '+10 de poder de habilidad
+25% de curación adicional de pociones
+100 de maná
Pasiva ÚNICA - Pavor: Otorga +3 de poder de habilidad por acumulación de Gloria.
Pasiva ÚNICA - Todo o nada: Otorga 2 de Gloria por asesinato de campeón y 1 de Gloria por asistencia, hasta un total de 10 de Gloria. Al morir se pierde 4 de Gloria.
Coste: 350'),
('Sombras gemelas', 'Aumenta el poder de habilidad y la velocidad de movimiento.', '+70 de poder de habilidad
+7% de velocidad de movimiento
+10% de reducción de enfriamiento
Activa ÚNICA - Persecución espectral: Invoca 2 fantasmas que persiguen a los campeones cercanos, los revela y los atormenta al tocarlos.
Los enemigos atormentados sufren una ralentización del 40% durante un máximo de 5 s, según la distancia que recorran los fantasmas (90 s de enfriamiento).
Coste: 650 (total 2400)'),
('Sombrero mortal de Rabadon', 'Aumenta enormemente el poder de habilidad.', '+120 poder de habilidad
Pasiva ÚNICA: Aumenta un 40% el poder de habilidad.
Coste: 1100 (total 3600)'),
('Sorbemaleficios', 'Aumenta el daño de ataque y la resistencia mágica.', '+20 daño de ataque
+35 resistencia mágica
Pasiva ÚNICA, Salvavidas: Al recibir daño mágico que reduzca la vida por debajo del 30%, otorga un escudo que absorbe de 110 a 280 puntos de daño mágico durante 5 s (90 s de enfriamiento).
Coste: 500 (total 1300)'),
('Subir moral', 'Andanada de cañón acelera a los aliados.
Requiere 500 Serpientes de Plata', 'Pasiva ÚNICA: Las unidades aliadas en la Andanada de cañón ganan un 30% de velocidad de movimiento durante 2 s.
Coste: 0'),
('Sudario glacial', 'Aumenta la armadura y la reducción de enfriamiento.', '+20 armadura
+250 maná
Pasiva ÚNICA: +10% reducción de enfriamiento
Coste: 250 (total 900)'),
('Tabi de ninja', 'Mejora la velocidad de movimiento y reduce el daño de los ataques básicos recibidos.
Limitado a 1 par de botas.', '+20 de armadura
Pasiva ÚNICA: Bloquea un 12% del daño de los ataques básicos.
Pasiva ÚNICA - Movimiento mejorado: +45 de velocidad de movimiento.
Coste: 500 (total 1100)'),
('Talismán del cazador', 'Proporciona daño contra monstruos y regeneración de maná en la jungla.', 'Pasiva ÚNICA - Diente: Al dañar a un monstruo con un hechizo o un ataque, le robas 30 de vida a lo largo de 5 s y le infliges 60 de daño mágico como quemadura. Mientras estés en la jungla o en el río, regeneras hasta 8 de maná por segundo en función del maná que te falte. Este objeto otorga Cazador de monstruos.
Limitado a 1 objeto de ganancia de oro o de jungla.
Coste: 350'),
('Tiamat', 'Los ataques cuerpo a cuerpo impactan en los enemigos cercanos.', '+25 daño de ataque
+50% regeneración de vida básica
Pasiva ÚNICA - Hender: Los ataques básicos infligen entre un 20% y un 60% del daño de ataque total como daño físico adicional a los enemigos que estén cerca del objetivo en el momento del impacto (cuanto más cerca estén los enemigos, más daño sufrirán).
Activa ÚNICA - Creciente: Inflige entre un 60% y un 100% del daño de ataque total como daño físico a las unidades enemigas cercanas (cuanto más cerca esté el enemigo, más daño sufrirá) (10 s de enfriamiento).
Coste: 475 (total 1325)'),
('Tomo amplificador', 'Aumenta levemente el poder de habilidad.', '+20 poder de habilidad
Coste: 435'),
('Tormento de Liandry', 'El daño de hechizos quema una porción de la vida de los enemigos.', '+75 de poder de habilidad
+300 de vida
Pasiva ÚNICA - Locura: Inflige un 2% más de daño por cada segundo en combate contra campeones (máximo un 10%).
Pasiva ÚNICA - Tormento: Los hechizos prenden fuego a los enemigos durante 3 s e infligen daño mágico adicional equivalente al 1.5% de su vida máxima por segundo. El daño de la quemadura aumenta al 2.5% contra unidades con movimiento reducido.
Coste: 750 (total 3100)'),
('Tótem guardián (talismán)', 'Coloca periódicamente un Guardián invisible.
Limitado a 1 Talismán.', 'Activa: Consume una carga para colocar un Guardián invisible que revela la zona circundante durante 90-120 s.
Almacena una carga cada 240-120 s, hasta un máximo de 2 cargas.
La duración del guardián y el tiempo de recarga mejoran progresivamente con el nivel.
(Limitado a 3 guardianes invisibles en el mapa por jugador).
Coste: 0'),
('Últimas palabras', 'Mayor eficacia contra enemigos que tengan mucha armadura.', '+20 de daño de ataque
Pasiva ÚNICA, Últimas palabras: +20% de penetración de armadura.
Coste: 750 (total 1450)'),
('Vara de las edades', 'Aumenta en gran medida la vida, el maná y el poder de habilidad.', '+300 de vida
+300 de maná
+60 de poder de habilidad
Pasiva: Otorga +20 de vida, +10 de maná y +4 de poder de habilidad por acumulación (máximo +200 de vida, +100 de maná y +40 de poder de habilidad). Otorga 1 acumulación por minuto (máximo de 10 acumulaciones).
Pasiva ÚNICA, Eternidad: Se obtiene como maná un 15% del daño sufrido de campeones. Al gastar maná se recupera un 20% del coste como vida, hasta un máximo de 25 por lanzamiento.
Coste: 650 (total 2600)'),
('Vara de las edades (carga rápida)', 'Aumenta en gran medida la vida, el maná y el poder de habilidad.', '+300 de vida
+300 de maná
+60 de poder de habilidad
Pasiva: Otorga +20 de vida, +10 de maná y +4 de poder de habilidad por acumulación (máximo +200 de vida, +100 de maná y +40 de poder de habilidad). Otorga 1 acumulación cada 40 s (máximo de 10 acumulaciones).
Pasiva ÚNICA - Eternidad: Se obtiene como maná un 15% del daño sufrido de campeones. Al gastar maná se recupera un 20% del coste como vida, hasta un máximo de 25 por lanzamiento.
Coste: 650 (total 2600)'),
('Vara explosiva', 'Aumenta moderadamente el poder de habilidad.', '+40 poder de habilidad
Coste: 850'),
('Vara innecesariamente grande', 'Aumenta en gran medida el poder de habilidad.', '+60 poder de habilidad
Coste: 1250'),
('Velo del hada de la muerte', 'Bloquea de vez en cuando las habilidades enemigas.', '+75 de poder de habilidad
+60 de resistencia mágica
+10% de reducción de enfriamiento
Pasiva ÚNICA - Escudo de hechizos: Otorga un escudo de hechizos que bloquea la siguiente habilidad enemiga. El escudo se regenera si no sufres daño de campeones enemigos durante 40 s.
Coste: 800 (total 3000)'),
('Vinculahechizos', 'Acumula los lanzamientos de hechizos cercanos y puede consumirlos para otorgar velocidad de movimiento y poder de habilidad.', '+120 de poder de habilidad
+10% de velocidad de movimiento
Pasiva ÚNICA: Los hechizos que lancen los aliados y enemigos cercanos cargan el Vinculahechizos hasta un límite (100 como máximo).
Activa ÚNICA: Obtienes un máximo de 80 de poder de habilidad y un 50% de velocidad de movimiento decreciente durante 4 s.
Cada lanzamiento de hechizo acumulado suma +0.8 de poder de habilidad y +0.5% de velocidad de movimiento al efecto (60 s de enfriamiento).
Coste: 800 (total 2900)'),
('Visión lejana modificada', 'Otorga más alcance y revela la zona seleccionada.
*Requiere nivel 9 o más para mejorarse.
Limitado a 1 talismán.', 'Modifica el talismán Tótem guardián:	
+ Aumenta considerablemente el alcance de lanzamiento (+650%)
+ Tiene duración infinita y no cuenta para el límite de guardianes
- 10% de aumento de enfriamiento
- El guardián es visible, frágil y los aliados no pueden marcarlo como objetivo
- 45% de reducción del radio de visión del guardián
- No puede acumular cargas
Coste: 0'),
('Yelmo adaptable', 'Reduce el daño de los hechizos y efectos repetidos.', '+350 de vida
+55 de resistencia mágica
+100% de regeneración de vida básica
+10% de reducción de enfriamiento
Pasiva ÚNICA: Recibir daño mágico de un hechizo o efecto reduce el daño mágico siguiente recibido de ese mismo hechizo o efecto un 20% durante 4 s.
Coste: 1000 (total 2800)');


select * from tb_items;

create table tb_runa(
id_runa INT AUTO_INCREMENT,
nom_runa varchar(30)not null,
des_runa varchar(500) not null,
img_runa varchar(300) null,
primary key(id_runa)
);

insert into tb_runa (nom_runa,des_runa) values 
('Precision','El Camino Rúnico de Precisión es uno de los 5 Caminos Rúnicos. Se centra en los ataques básicos, la durabilidad y el daño.'),
('Dominacion','El Camino Rúnico de Dominación es uno de los 5 Caminos Rúnicos. Se centra en daño de ráfaga, y utilidad general.'),
('Brujeria','El Camino Rúnico de Brujería es uno de los 5 Caminos Rúnicos. Se centra en daño de la habilidad y utilidad general.'),
('Valor','El Camino Rúnico de Valor es uno de los cinco caminos rúnicos. Se centra en las resistencias, la salud y la curación.'),
('Inspiracion','El Camino Rúnico de Inspiración es uno de los cinco caminos rúnicos. Se centra en herramientas únicas, generación de oro, y objetos.');

select * from tb_runa;
-- -

create table tb_runa_clave(
id_runa_clave INT AUTO_INCREMENT,
nom_runa_clave varchar(30)not null,
nota1_runa_clave varchar(500) not null,
nota2_runa_clave varchar(500) not null,
id_runa INT,
img_runa_clave varchar(300) null,
primary key(id_runa_clave),
foreign key (id_runa) references tb_runa(id_runa)
);

insert into tb_runa_clave(nom_runa_clave,nota1_runa_clave,nota2_runa_clave,id_runa) values
('Estrategia Ofensiva','Los efectos que te permiten aplicar más de un efecto de impacto por ataque también aplican acumulaciones adicionales de Estrategia Ofensiva (p. ej. Espadafuria de Guinsoo Espadafuria de Guinsoo, Depredador Implacable Depredador Implacable, Mordida Gemela Mordida Gemela).','El ataque que activa Estrategia Ofensiva no se beneficia del aumento de daño.
Sin embargo, cualquier Blaze.png daño prolongado o efecto subsiguiente que venga con el ataque básico se verá afectado (p. ej. Hemorragia Hemorragia, Tiro Tóxico Tiro Tóxico y todos los Golpes de Tentáculo Golpes de Tentáculo subsiguientes).',1),
('Cadencia Letal','El efecto es capaz de eliminar el límite de velocidad de ataque de Gloria en la Muerte Gloria en la Muerte de Sion Sion pero no puede eliminar el de Jhin Jhin o Graves Graves.','-',1),
('Electrocutar','PASIVA: Los ataques básicos y las habilidades generan acumulaciones al impactar Champion icon.png campeones enemigos, hasta uno por ataque o lanzamiento. La aplicación de 3 acumulaciones a un objetivo dentro de un período de 3 segundos hace que sea golpeado por un rayo, causando 30 − 180 (según el nivel) (+ 40% DA adicional) (+ 25% PH) de Attack damage.pngAbility power.png Daño adaptable.', 'DAÑO ADAPTABLE: Inflige Attack damage.png daño físico o Ability power.png daño mágico dependiendo en tus estadísticas adicionales, prefijando en Attack damage.png daño físico.',2);
select * from tb_runa_clave;

insert into tb_runa_clave(nom_runa_clave,nota1_runa_clave,nota2_runa_clave,id_runa) values
('Aumento Glacial',
'PASIVA – AUMENTO GLACIAL: Los ataques basicos Slow icon.png ralentizan a los campeones enemigos durante 2 segundos, lo que aumenta con la duración. 
Este efecto no puede ocurrir en el mismo objetivo más de una vez cada pocos segundos.
Melee - campeones cuerpo a cuerpo: 45% − 55% (según el nivel) de ralentización máxima.
Ranged - campeones a distancia: 30% − 40% (según el nivel) de ralentización máxima.',
'PASIVA – RAYO FRÍO: Slow icon.png ralentizar un campeón enemigo con un objeto activo crea una línea congelada de 600 unidades en su dirección durante 5 segundos, 
ralentizando en un 60% a todos los enemigos dentro. El Rayo Frío puede dispararse a cualquier cantidad de campeones al mismo tiempo, y no tiene tiempo de enfriamiento.',5);

create table tb_runa_clave_secundaria(
id_runa_clave_sec INT AUTO_INCREMENT,
id_runa INT,
primary key(id_runa_clave_sec),
foreign key (id_runa) references tb_runa(id_runa)
);

insert into tb_runa_clave_secundaria(id_runa) values
(3);

insert into tb_runa_clave_secundaria(id_runa) values
(2);

select * from tb_runa_clave_secundaria;

create table tb_runa_ranuras(
id_runa_ranuras INT AUTO_INCREMENT,
nom_runa_ranuras varchar(30)not null,
nota1_runa_ranuras varchar(500) not null,
nota2_runa_ranuras varchar(500) not null,
id_runa INT,
img_runa_clave varchar(300) null,
primary key(id_runa_ranuras),
foreign key (id_runa) references tb_runa(id_runa)
);

insert into tb_runa_ranuras(nom_runa_ranuras,nota1_runa_ranuras,nota2_runa_ranuras,id_runa) values 
('Sabor a Sangre','Si se activa siempre que esté disponible, Sabor a Sangre restaura teóricamente 0.9 − 1.75 (según el nivel) (+ 1% DA adicional) (+0.5% PH) de vida cada segundo.','-',2),
('Colección de Ojos','Completar tu Colección de Ojos te otorga un total de 18 DA adicional o 30 PH (adaptable).','-',2),
('Cazador Voraz','Te curas por 1.5% (+ 2.5% por acumulación de Cazarrecompensas) del daño infligido por tus habilidades, efectos de impacto y objetos, hasta un 14% a 5 acumulaciones. Se beneficia de Curación.','Ganas una acumulación de Cazarrecompensas por derribo de campeón, hasta uno por cada campeón enemigo único.',2),
('Anillo de Flujo de Mana','PASIVA: Golpear a un Champion icon.png campeón enemigo con una habilidad aumenta permanentemente tu maná máximo por 25, hasta 250 de maná.','Después de alcanzar la bonificación de 250 de maná, restauras permanentemente 1% de tu maná máximo cada 5 segundos.',3),
('Trascendencia','PASIVA: Obtienes 10% de reducción de enfriamiento en el nivel 10.','Además, ganas 1.2 DA adicional o 2 PH (adaptable) por cada 1% de reducción de enfriamiento en exceso del límite.',3);
select * from tb_runa_ranuras;

-- drop table tb_runas_recomendadas;
create table tb_runas_recomendadas(
id_runa_reco INT AUTO_INCREMENT,
nom_runa_comb varchar(100)not null,
pick_rate_runa_reco varchar(50) not null,
win_rate_runa_reco varchar(50) not null,
id_per int,
id_pro int,
id_runa_clave int,
id_runa_clave_sec int,
fragmento_ranura1 varchar(100)not null,
fragmento_ranura2 varchar(100)not null,
fragmento_ranura3 varchar(100)not null,
primary key(id_runa_reco),
foreign key (id_per) references tb_personaje(id_per), -- personaje
foreign key (id_pro) references tb_profesional(id_pro), -- profesional
foreign key (id_runa_clave) references tb_runa_clave(id_runa_clave), -- runa clave
foreign key (id_runa_clave_sec) references tb_runa_clave_secundaria(id_runa_clave_sec) -- runa clave
);

select * from tb_runas_recomendadas;
-- delete from tb_runas_recomendadas
-- where id_runa_reco=4;

insert into tb_runas_recomendadas(nom_runa_comb,pick_rate_runa_reco,win_rate_runa_reco,id_per,id_pro,id_runa_clave,id_runa_clave_sec,fragmento_ranura1,fragmento_ranura2,fragmento_ranura3) values 
('Dominacion + Brujeria','52.41%','47.68%',1,1,4,1,'Ofensivo: +10% velocidad de ataque','Ofensivo: +09 fuerza adaptable','flexible: +08 resistencia magica');
select * from tb_runa_clave;
select * from tb_runas_recomendadas;
/*update tb_runas_recomendadas
set id_runa_clave=5
where id_runa_reco=1;

update tb_runas_recomendadas
set id_runa_clave_sec=5
where id_runa_reco=1;*/

-- drop table tb_runas_recomendadas_claves;
create table tb_runas_recomendadas_claves(
num_runa_rec_clave INT,
id_runa_reco INT,
id_runa_ranuras INT,
seccion_ranuras varchar(200)not null,
primary key(num_runa_rec_clave,id_runa_reco,id_runa_ranuras),
foreign key (id_runa_reco) references tb_runas_recomendadas(id_runa_reco),
foreign key (id_runa_ranuras) references tb_runa_ranuras(id_runa_ranuras)
);

insert into tb_runas_recomendadas_claves(num_runa_rec_clave,id_runa_reco,id_runa_ranuras,seccion_ranuras) values (1,1,1,'primaria'),(1,1,2,'primaria'),(1,1,3,'primaria');
select * from tb_runas_recomendadas_claves;

create table tb_runas_recomendadas_secundarias(
num_runa_rec_clave_sec INT,
id_runa_reco INT,
id_runa_ranuras INT,
seccion_ranuras varchar(200)not null,
primary key(num_runa_rec_clave_sec,id_runa_reco,id_runa_ranuras),
foreign key (id_runa_reco) references tb_runas_recomendadas(id_runa_reco),
foreign key (id_runa_ranuras) references tb_runa_ranuras(id_runa_ranuras)
);

insert into tb_runas_recomendadas_secundarias(num_runa_rec_clave_sec,id_runa_reco,id_runa_ranuras,seccion_ranuras) values (1,1,4,'secundaria'),(1,1,5,'secundaria');
select * from tb_runas_recomendadas_secundarias;

select a.*,b.*,c.*,d.*,e.*,f.*,g.*
from tb_runas_recomendadas a 
join tb_profesional b on a.id_pro=b.id_pro
join tb_personaje c on a.id_per=c.id_per
join tb_runa_clave d on a.id_runa_clave = d.id_runa_clave
join tb_runas_recomendadas_claves e on a.id_runa_reco = e.id_runa_reco
join tb_runa f on f.id_runa = d.id_runa
join tb_runa_clave_secundaria g
where a.id_pro=1;

select 
a.*,b.*,c.*,d.*,e.*,f.*
from tb_runas_recomendadas a 
join tb_profesional b on a.id_pro=b.id_pro
join tb_personaje c on a.id_per=c.id_per
join tb_runa_clave d on a.id_runa_clave = d.id_runa_clave
join tb_runas_recomendadas_secundarias e on a.id_runa_reco = e.id_runa_reco
join tb_runa f on d.id_runa = f.id_runa
where a.id_pro=1;

select a.id_runa_reco, a.nom_runa_comb, a.pick_rate_runa_reco, a.win_rate_runa_reco, b.nom_per, c.nom_pro, e.nom_runa as 'nom_runa_pri', g.nom_runa as 'nom_runa_sec',
a.fragmento_ranura1,a.fragmento_ranura2,a.fragmento_ranura3
from tb_runas_recomendadas a 
join tb_personaje b on a.id_per = b.id_per
join tb_profesional c on a.id_pro = c.id_pro
join tb_runa_clave d on a.id_runa_clave = d.id_runa_clave
join tb_runa e on d.id_runa = e.id_runa
join tb_runa_clave_secundaria f on a.id_runa_clave_sec = f.id_runa_clave_sec
join tb_runa g on f.id_runa = g.id_runa
where b.nom_per like '%leblanc%' and c.nom_pro like '%Sang%';

select * from tb_items;

-- tabla de counters, de prueba
create table tb_counter(
id_counter INT AUTO_INCREMENT,
id_per INT,
gana_lin_contra varchar(200)not null,
pierde_lin_contra varchar(200)not null,
mas_fuerte_contra varchar(200)not null,
gana_mas_contra varchar(200)not null,
pierde_mas_contra varchar(200)not null,
primary key(id_counter),
foreign key (id_per) references tb_personaje(id_per)
);

insert into tb_counter(id_per,gana_lin_contra,pierde_lin_contra,mas_fuerte_contra,gana_mas_contra,pierde_mas_contra)
values (3,'GALIO','IRELIA','ILLAOI','AZIR','VELKOZ');

SELECT * FROM TB_COUNTER;


--prueba

